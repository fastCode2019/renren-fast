package io.renren.common.utils;

import io.renren.common.exception.RRException;

import java.util.HashMap;
import java.util.Map;

public  class ApiParamFix {
    // 红鸭微信小程序接口
    public static Map<String, String> FixRadYaParam(Map<String, String> ordParam, Boolean wrap ) {
        Map<String, String> param = new HashMap<>();
        if(ordParam.containsKey("关注名称")){
            param.put("advertInfoTitle", ordParam.get("关注名称"));
            param.put("wppName", ordParam.get("关注名称"));
        }
        if(ordParam.containsKey("定时(小时)")){
            try{
                Integer num = Integer.parseInt(ordParam.get("定时(小时)"));
                param.put("seconds", "" + (num * 60) );
            }catch (Exception e){
                throw new RRException("定时(小时)参数错误");
            }
        }
        if(ordParam.containsKey("任务类型")){
            if(!ordParam.get("任务类型").equals("定时"))
                param.put("seconds", "60");
        }
        if(ordParam.containsKey("数量")){
            param.put("advertInfoReadNumMax", ordParam.get("数量"));
        }
        if(ordParam.containsKey("二维码")){
            param.put("QEcodeImg", "0");
            if(!ordParam.containsKey("advertInfoUrl")){
                param.put("advertInfoUrl", "");
            }
        }
        if(ordParam.containsKey("文章标题")){
            param.put("advertInfoTitle", ordParam.get("文章标题"));
            param.put("wppName", "");
        }
        if(ordParam.containsKey("链接")){
            param.put("advertInfoUrl", ordParam.get("链接"));
            param.remove("QEcodeImg");
        }
        if (wrap)
            param.putAll(ordParam);

        param.put("urlSource", "1");
        param.put("likeNumMax", "0");
        param.put("advertNumMax", "0");
        param.put("readOriginalNumMax", "0");
        param.put("readCommentLikeNumMax", "0");
        param.put("readCommentNumMax", "0");
        param.put("readShareNumMax", "0");
        param.put("readCollectionNumMax", "0");
        param.put("readFollowNumMax", "0");
        param.put("clickRate", "100");
        param.put("sex", "1");
        param.put("lexiconCount", "");
        param.put("reserveCteateTime", "");
        param.put("describe", "");
        param.put("sourceRatio", "100,0,0,0");
        System.out.println(param);
        return param;
    }
}
