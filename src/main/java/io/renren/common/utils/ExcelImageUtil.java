package io.renren.common.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.renren.common.exception.RRException;
import io.renren.modules.redDuck.entity.OrderFrom;
import org.apache.poi.POIXMLDocumentPart;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFPicture;
import org.apache.poi.hssf.usermodel.HSSFPictureData;
import org.apache.poi.hssf.usermodel.HSSFShape;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.PictureData;
import org.apache.poi.xssf.usermodel.*;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTMarker;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

/**
 * 从excel中取出图片的功能代码
 * @author Administrator
 * 2019-11-26
 */
public class ExcelImageUtil {

    private final static String Excel_2003 = ".xls"; //2003 版本的excel
    private final static String Excel_2007 = ".xlsx"; //2007 版本的excel

	public static void main(String[] args) throws Exception {

	}

	public static List<OrderFrom> getOrderForms(InputStream inStr, String fileName) throws Exception {
		ImportExcelUtil util = new ImportExcelUtil();
		String fileType = fileName.substring(fileName.lastIndexOf("."));

		if (Excel_2003.equals(fileType)) {
			HSSFWorkbook work = new HSSFWorkbook(inStr);//2003 版本的excel
			List<List<Object>> rows = util.getBankListByExcel(work);
			Map<String, PictureData> pict = getPicturesHSS(work.getSheetAt(0));

			return parseOrderForms(rows, pict);
		} else if (Excel_2007.equals(fileType)) {
			XSSFWorkbook work = new XSSFWorkbook(inStr);//2007 版本的excel
			List<List<Object>> rows = util.getBankListByExcel(work);
			Map<String, PictureData> pict = getPicturesXSS(work.getSheetAt(0));
			return parseOrderForms(rows, pict );
		} else {
			throw new Exception("解析文件格式有误！");
		}
	}

     /**
	   * 获取图片和位置 (xls)测试过
	   * @param sheet
	   * @return
	   * @throws IOException
	   */
	  public static Map<String, PictureData> getPicturesHSS (HSSFSheet sheet) throws IOException {
		    Map<String, PictureData> map = new HashMap<String, PictureData>();
		    if(sheet.getDrawingPatriarch() == null){
		    	return map;
			}
		    List<HSSFShape> list = sheet.getDrawingPatriarch().getChildren();
		    for (HSSFShape shape : list) {
		        if (shape instanceof HSSFPicture) {
		            HSSFPicture picture = (HSSFPicture) shape;
		            HSSFClientAnchor cAnchor = (HSSFClientAnchor) picture.getAnchor();
		            HSSFPictureData pdata = picture.getPictureData();
		            String key = cAnchor.getRow1() + ""; // 行号-列号
		            map.put(key, pdata);
		        }
		    }
		    return map;
		}
	   
	  /**
	   * 获取图片和位置 (xlsx)
	   * @param sheet
	   * @return
	   * @throws IOException
	   */
	public static Map<String, PictureData> getPicturesXSS (XSSFSheet sheet) throws IOException {
	  	Map<String, PictureData> map = new HashMap<String, PictureData>();
	  	List<POIXMLDocumentPart> list = sheet.getRelations();
	  	for (POIXMLDocumentPart part : list) {
	  		if (part instanceof XSSFDrawing) {
			  	XSSFDrawing drawing = (XSSFDrawing) part;
			  	List<XSSFShape> shapes = drawing.getShapes();
			  	for (XSSFShape shape : shapes) {
				  	XSSFPicture picture = (XSSFPicture) shape;
				  	XSSFClientAnchor anchor = picture.getPreferredSize();
				  	CTMarker marker = anchor.getFrom();
				  	String key = marker.getRow() + "";
				  	map.put(key, picture.getPictureData());
		  		}
		  	}
		  }
	      return map;
  	}
	private static List<OrderFrom> parseOrderForms(List<List<Object>> rows, Map<String, PictureData> pict) throws IOException {
		List<OrderFrom> orderForms = new ArrayList<>();

		if(rows.isEmpty()){
			throw new RRException("请填写表格");
		}
		if(pict.isEmpty()){
			for (int i= 1 ; i<rows.size(); i++){
				List<Object> row = rows.get(i);
				List<Object> header = rows.get(0);
				OrderFrom from = new OrderFrom();
				Map<String, String > params = new HashMap<>();
				List<MultipartFile> files = new ArrayList<>();
				for(int h = 0; h< header.size(); h++){
					String head = header.get(h).toString();
					if (head.equals("二维码")) {
						files.add(HttpUtils.down((String) row.get(h)));
					}
					params.put(head, (String) row.get(h));
				}
				from.setParams(params);
				from.setFiles(files);
				orderForms.add(from);
			}
		}else{
			for (String key: pict.keySet()) {
				PictureData pic = pict.get(key);
				Integer i = Integer.parseInt(key);
				if(i >= rows.size()){
					throw new RRException("二维码和表格数据不匹配");
				}
				List<Object> row = rows.get(i);
				List<Object> header = rows.get(0);
				OrderFrom from = new OrderFrom();
				Map<String, String > params = new HashMap<>();
				for(int h = 0; h< header.size(); h++){
					params.put(header.get(h).toString(), (String) row.get(h));
				}
				MockMultipartFile mock = new MockMultipartFile("QEcodeImg", "pic." + pic.suggestFileExtension(), pic.getMimeType(), pic.getData());
				List<MultipartFile> files = new ArrayList<>();
				files.add(mock);
				from.setParams(params);
				from.setFiles(files);
				orderForms.add(from);
			}
		}
		return orderForms;
	}
	private static List<OrderFrom> parseOrderFrom(List<List<Object>> rows, Map<String, PictureData> pict){
		List<OrderFrom> orderForms = new ArrayList<>();

		if(rows.isEmpty()){
			throw new RRException("请填写表格");
		}

		for (String key: pict.keySet()) {
			PictureData pic = pict.get(key);
			Integer i = Integer.parseInt(key);
			if(i >= rows.size()){
				throw new RRException("二维码和表格数据不匹配");
			}
			List<Object> row = rows.get(i);
			OrderFrom from = new OrderFrom();
			Map<String, String > params = new HashMap<>();
			params.put("advertInfoTitle", (String) row.get(1));
			params.put("wppName", (String) row.get(1));
			String type = (String) row.get(2);
			params.put("type", type);
			if(type.startsWith("定时")){
				Integer sec = Integer.parseInt((String) row.get(3));
				params.put("seconds", String.valueOf(sec * 60));
			}else{
				params.put("seconds", String.valueOf(1 * 60));
			}
			params.put("advertInfoReadNumMax", (String) row.get(4));
			params.put("QEcodeImg", "0");
			MockMultipartFile mock = new MockMultipartFile("QEcodeImg", "pic." + pic.suggestFileExtension() , pic.getMimeType(), pic.getData());
			List<MultipartFile> files = new ArrayList<>();
			files.add(mock);
			from.setParams(params);
			from.setFiles(files);
			orderForms.add(from);
		}
		return orderForms;
	}

}

