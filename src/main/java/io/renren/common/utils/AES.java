package io.renren.common.utils;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.Security;
import java.util.*;

public class AES {
    private static String iv = "asdewqrf";
    private static final String SecretKey="keycansr";
    private static String CipherMode = "DES/CBC/PKCS7Padding";
    private static final String EncryptAlg ="DES";
    private static final int SecretKeySize=8;
    private static final Integer IVSize=8;
    private static final String Encode="UTF-8";
    /**
     * 创建密钥
     * @return
     */
    private static SecretKeySpec createKey(){
        StringBuilder sb= new StringBuilder(SecretKeySize);
        sb.append(SecretKey);
        if (sb.length()>SecretKeySize){
            sb.setLength(SecretKeySize);
        }
        if (sb.length()<SecretKeySize){
            while (sb.length()<SecretKeySize){
                sb.append(" ");
            }
        }
        try {
            byte[] data = sb.toString().getBytes(Encode);
            return new SecretKeySpec(data, EncryptAlg);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
    /**
     * 创建16位向量: 不够则用0填充
     * @return
     */
    private static IvParameterSpec createIV() {
        StringBuffer sb = new StringBuffer(IVSize);
        sb.append(iv);
        if (sb.length()>IVSize){
            sb.setLength(IVSize);
        }
        if (sb.length()<IVSize){
            while (sb.length()<IVSize){
                sb.append("0");
            }
        }
        byte[] data = null;
        try {
            data=sb.toString().getBytes(Encode);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return new IvParameterSpec(data);
    }

    /**
     * 加密：有向量16位，结果转base64
     * @param context
     * @return
     */
    public static String encrypt(String context) {
        try {
            //下面这行在进行PKCS7Padding加密时必须加上，否则报错
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

            byte[] content = context.getBytes(Encode);
            SecretKeySpec key = createKey();
            Cipher cipher = Cipher.getInstance(CipherMode);
            cipher.init(Cipher.ENCRYPT_MODE, key, createIV());
            byte[] data = cipher.doFinal(content);
            String result= Base64.encodeBase64String(data);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String decrypt(String context) {
        try {
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
            byte[] data=Base64.decodeBase64(context);
            SecretKeySpec key = createKey();
            Cipher cipher = Cipher.getInstance(CipherMode);
            cipher.init(Cipher.DECRYPT_MODE, key, createIV());
            byte[] content = cipher.doFinal(data);
            String result=new String(content,Encode);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public static String mapSortToString(Map<String, String> map){
            String params = "";
            List<String> keys = new ArrayList<>();
            for (String key : map.keySet()){
                keys.add(key);
            }

            Collections.sort(keys);
            for (int i =0; i< keys.size(); i++) {
                String key = keys.get(i);
                params = params + key +"=" + map.get(key);
                if(i < keys.size() - 1){
                    params += ",";
                }
            }
            System.out.println(params);
            return params;
    }
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        map.put("advertInfoUid","b6700d8bca3b42a8808a34f2410bc562");
        map.put("page","1");
        map.put("limit","100");
        map.put("timestamp","1606224112140");
        map.put("search","");
        map.put("state","0");
        String params = mapSortToString(map);
        String sgin = AES.encrypt(params);
        System.out.println(sgin);

        map.put("sign",sgin);
        String result = HttpUtils.post("https://api.redya.cc/api/advert/advertInfo/list",map);
        System.out.println(result);
    }
}
