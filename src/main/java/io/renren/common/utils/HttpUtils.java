package io.renren.common.utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Map;

public class HttpUtils {
    private static final int REQUEST_TIMEOUT = 10 * 1000; // 设置请求超时10秒钟

    private static final int CONNECT_TIMEOUT = 10 * 1000; // 连接超时时间

    private static final int SO_TIMEOUT = 15 * 1000; // 数据传输超时

    private static final String ENCODING = "UTF-8";

    // 务必单例
    private static CloseableHttpClient client;

    static {
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(CONNECT_TIMEOUT)
                .setConnectionRequestTimeout(REQUEST_TIMEOUT)
                .setSocketTimeout(SO_TIMEOUT)
                .build();
        client = HttpClients.custom().setDefaultRequestConfig(requestConfig).setMaxConnTotal(50).build();
    }


    public static String get(String url) {
        return send(RequestBuilder.get(url));
    }
    public static MultipartFile down(String url) throws IOException {
        if(url.isEmpty()){
            return null;
        }
        String[] suffixs = url.split("\\.");
        String suffix = "";
        if (suffixs.length > 0){
            suffix = suffixs[suffixs.length - 1];
        }else{
            return null;
        }
        String[] format = new String[]{"JPG","JPEG", "PNG", "GIF", "BMP"};
        if(!Arrays.asList(format).contains(suffix.toUpperCase())){
            return null;
        }
        HttpEntity http = down(RequestBuilder.get(url));

        return new MockMultipartFile("QEcodeImg", "img." + suffix, http.getContentType().getValue(), http.getContent());
    }

    public static String get(String url, Map<String, String> paramsMap) {
        return send(RequestBuilder.get(url), paramsMap);
    }
    public static String post(String url, String body) {
        return send(RequestBuilder.post(url).setHeader("content-type","application/x-www-form-urlencoded"), body);
    }
    public static String post(String url, Map<String, String> paramsMap) {
        return send(RequestBuilder.post(url), paramsMap);
    }
    public static String post(String url, Map<String, String> paramsMap, Map<String, MultipartFile> fileMap) {
        return sendFile(RequestBuilder.post(url), fileMap , paramsMap);
    }


    public static String send(RequestBuilder requestBuilder, String body) {
        requestBuilder.setCharset(Charset.forName(ENCODING));

        String responseText = "";

        CloseableHttpResponse response = null;
        try {
            requestBuilder.setEntity(new StringEntity(body));

            response = client.execute(requestBuilder.build());
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    responseText = EntityUtils.toString(entity, ENCODING);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();//正式项目中请改为log打印
        } finally {
            try {
                response.close();
            } catch (Exception e) {
                e.printStackTrace();//正式项目中请改为log打印
            }
        }
        return responseText;
    }

    public static String send(RequestBuilder requestBuilder) {
        requestBuilder.setCharset(Charset.forName(ENCODING));

        String responseText = "";

        CloseableHttpResponse response = null;
        try {
            response = client.execute(requestBuilder.build());
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    responseText = EntityUtils.toString(entity, ENCODING);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();//正式项目中请改为log打印
        } finally {
            try {
                response.close();
            } catch (Exception e) {
                e.printStackTrace();//正式项目中请改为log打印
            }
        }
        return responseText;
    }

    public static HttpEntity down(RequestBuilder requestBuilder) {
        requestBuilder.setCharset(Charset.forName(ENCODING));
        CloseableHttpResponse response = null;
        HttpEntity entity = null;
        try {
            response = client.execute(requestBuilder.build());
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                 entity = response.getEntity();
                if(entity != null){
                    entity = new BufferedHttpEntity(entity);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();//正式项目中请改为log打印
        } finally {
            try {
                response.close();
            } catch (Exception e) {
                e.printStackTrace();//正式项目中请改为log打印
            }
        }
        return entity;
    }

    public static String send(RequestBuilder requestBuilder, Map<String, String> paramsMap) {
        requestBuilder.setCharset(Charset.forName(ENCODING));
        String responseText = "";

        if (paramsMap != null) {
            for (Map.Entry<String, String> param : paramsMap.entrySet()) {
                requestBuilder.addParameter(param.getKey(), param.getValue());
            }

            CloseableHttpResponse response = null;
            try {
                response = client.execute(requestBuilder.build());
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    HttpEntity entity = response.getEntity();
                    if (entity != null) {
                        responseText = EntityUtils.toString(entity, ENCODING);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();//正式项目中请改为log打印
            }finally {
                try {
                    response.close();
                } catch (Exception e) {
                    e.printStackTrace();//正式项目中请改为log打印
                }
            }

        }
        return responseText;

    }

    //这是设置buider编码格式的，不然传过去的文件名会乱码，HttpMultipartMode这个类很关键

    //https://www.cnblogs.com/evasean/archive/2018/07/25/9368670.html

    //https://blog.csdn.net/u012685794/article/details/51755799，这是两篇解释的博客

    public static String sendFile(RequestBuilder requestBuilder, Map<String, MultipartFile> fileMap, Map<String, String> paramsMap){
        String result = "";
        CloseableHttpResponse response = null;
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setCharset(Charset.forName(ENCODING));
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        for (Map.Entry<String, String> param : paramsMap.entrySet()) {
            requestBuilder.addParameter(param.getKey(), param.getValue());
        }
        try {
            for (Map.Entry<String, MultipartFile> file : fileMap.entrySet()) {
                builder.addBinaryBody(
                        file.getKey(),
                        file.getValue().getInputStream(),
                        ContentType.parse(file.getValue().getContentType()),
                        file.getValue().getOriginalFilename());// 文件流
            }
            if(!fileMap.isEmpty()){
                requestBuilder.setEntity(builder.build());
            }
            response = client.execute(requestBuilder.build());// 执行提交
            HttpEntity responseEntity = response.getEntity();
            if (responseEntity != null) {
                // 将响应内容转换为字符串
                result = EntityUtils.toString(responseEntity, Charset.forName(ENCODING));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                response.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

}
