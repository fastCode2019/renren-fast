package io.renren.common.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.renren.common.exception.RRException;

import java.util.Date;
import java.util.Map;

import static io.renren.common.utils.AES.mapSortToString;

public class RedYaApi {

    public static JSONObject post(String url, Map<String, String> map){
        map.put("timestamp", new Date().getTime() + "");
        String params = mapSortToString(map);
        String sgin = AES.encrypt(params);
        map.put("sign",sgin);
        String result = HttpUtils.post(url, map);
        JSONObject json = JSON.parseObject(result);
        if (json == null) {
            throw new RRException("服务接口调用失败");
        }
        if(json.getInteger("code") == 10001){
            json.put("code" , 0);
            json.put("msg", json.getString("message"));
        }else{
            json.put("msg", json.getString("message"));
        }
        return json;
    }
}
