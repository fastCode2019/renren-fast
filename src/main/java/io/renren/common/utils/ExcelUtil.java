package io.renren.common.utils;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import io.renren.common.exception.RRException;
import io.renren.modules.redDuck.entity.OrderFrom;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.*;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;
import static org.apache.poi.ss.usermodel.Workbook.PICTURE_TYPE_JPEG;

/**
 * Created by cdw on 2018/04/19.
 *
 * Apache POI操作Excel对象 HSSF：操作Excel 2007之前版本(.xls)格式,生成的EXCEL不经过压缩直接导出
 * XSSF：操作Excel 2007及之后版本(.xlsx)格式,内存占用高于HSSF SXSSF:从POI3.8
 * beta3开始支持,基于XSSF,低内存占用,专门处理大数据量(建议)。
 *
 * 注意: 值得注意的是SXSSFWorkbook只能写(导出)不能读(导入)
 *
 * 说明: .xls格式的excel(最大行数65536行,最大列数256列) .xlsx格式的excel(最大行数1048576行,最大列数16384列)
 * 这里引用的是阿里的json包，也可以自行转换成net.sf.json.JSONArray net.sf.json.JSONObject
 */
public class ExcelUtil {

    private final static String Excel_2003 = ".xls"; // 2003 版本的excel
    private final static String Excel_2007 = ".xlsx"; // 2007 版本的excel

    public static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd HH:mm:ss"; // 默认日期格式（类型为Date即可转换）
    public static final int DEFAULT_COLUMN_WIDTH = 17; // 默认列宽

    /**
     * 导入Excel
     *
     * @param file
     *            输入文件流
     */
    public static List<OrderFrom> importExcel(@RequestParam(value = "file", required = false) MultipartFile file)
            throws Exception {
        String fileName = file.getOriginalFilename();
        String xls = fileName.substring(fileName.lastIndexOf('.'));
        if (Excel_2003.equals(xls) || Excel_2007.equals(xls)) {
            return ExcelUtil.getImportExcel(file);
        } else {
            // 导入格式不正确
            throw new RRException("导入格式不正确：导入失败！");
        }
    }

    /**
     * 导出Excel
     *
     * @param titleList
     *            表格头信息集合
     * @param dataArray
     *            数据数组
     * @param os
     *            文件输出流
     */
    public static void exportExcel(ArrayList<LinkedHashMap> titleList, JSONArray dataArray, OutputStream os)
            throws Exception {
        ExcelUtil.getExportExcel(titleList, dataArray, os);
    }

    /**
     * 导入Excel
     *
     * @param file
     *            导入文件流对象
     */
    private static List<OrderFrom> getImportExcel(MultipartFile file) throws Exception {
        String fileName = file.getOriginalFilename();
        InputStream inputStream = file.getInputStream();
        // 获取工作模板行数据对象
        return  ExcelImageUtil.getOrderForms(inputStream, fileName);
    }

    /**
     * 导出Excel
     *
     * @param titleList
     *            表格头信息集合
     * @param dataArray
     *            数据数组
     * @param os
     *            文件输出流
     */
    private static void getExportExcel(ArrayList<LinkedHashMap> titleList, JSONArray dataArray, OutputStream os)
            throws Exception {
        String datePattern = DEFAULT_DATE_PATTERN;
        int minBytes = DEFAULT_COLUMN_WIDTH;

        /**
         * 声明一个工作薄
         */
        HSSFWorkbook workbook = new HSSFWorkbook();// 大于1000行时会把之前的行写入硬盘

        LinkedHashMap<String, String> headMap = titleList.get(0);

//        /**
//         * 生成一个(带名称)表格
//         */
        HSSFSheet sheet = workbook.createSheet("sheet");
//        sheet.createFreezePane(0, 3, 0, 3);// (单独)冻结前三行

        /**
         * 生成head相关信息+设置每列宽度
         */
        int[] colWidthArr = new int[headMap.size()];// 列宽数组
        String[] headKeyArr = new String[headMap.size()];// headKey数组
        String[] headValArr = new String[headMap.size()];// headVal数组
        int i = 0;
        for (Map.Entry<String, String> entry : headMap.entrySet()) {
            headKeyArr[i] = entry.getKey();
            headValArr[i] = entry.getValue();

            int bytes = headKeyArr[i].getBytes().length;
            colWidthArr[i] = bytes < minBytes ? minBytes : bytes;
            sheet.setColumnWidth(i, colWidthArr[i] * 256);// 设置列宽
            i++;
        }
//        SXSSFDrawing patriarch = sheet.createDrawingPatriarch();
        /**
         * 遍历数据集合，产生Excel行数据，除去 title + head 数据起始行为0，赋值为3（即第四行起）
         */
        int rowIndex = 0;
        for (Object obj : dataArray) {
            // 生成title+head信息
            if (rowIndex == 0) {

//                SXSSFRow title2Row = (SXSSFRow) sheet.createRow(1);// title2行
//                title2Row.createCell(0).setCellValue(title2);

//                CreationHelper createHelper = workbook.getCreationHelper();
//                XSSFHyperlink hyperLink = (XSSFHyperlink) createHelper.createHyperlink(HyperlinkType.URL);
//                hyperLink.setAddress(title2);
//                title2Row.getCell(0).setHyperlink(hyperLink);// 添加超链接
//
//                title2Row.getCell(0).setCellStyle(title2Style);

                Row headerRow =  sheet.createRow(0);// head行
                for (int j = 0; j < headValArr.length; j++) {
                    headerRow.createCell(j).setCellValue(headValArr[j]);
                }
                rowIndex = 1;
            }

            JSONObject jo = (JSONObject) JSONObject.toJSON(obj);
            // 生成数据
            Row dataRow =  sheet.createRow(rowIndex);// 创建行
            for (int k = 0; k < headKeyArr.length; k++) {

//                XSSFClientAnchor anchor = new XSSFClientAnchor(10, 10, 40, 40, (short)0, (short)rowIndex, (short)0, (short)rowIndex + 1);
//                ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
//                BufferedImage bufferImg = ImageIO.read(new File("D:\\ok.jpg"));
//                ImageIO.write(bufferImg, "jpg", byteArrayOut);
//                byte[] imgtypes = byteArrayOut.toByteArray();
//                int puctureIndex = workbook.addPicture(imgtypes, PICTURE_TYPE_JPEG);
//                patriarch.createPicture(anchor, puctureIndex);

                Cell cell = dataRow.createCell(k);// 创建单元格

                Object o = jo.get(headKeyArr[k]);
                String cellValue = "";

                if (o == null) {
                    cellValue = "";
                } else if (o instanceof Date) {
                    cellValue = new SimpleDateFormat(datePattern).format(o);
                } else if (o instanceof Float || o instanceof Double) {
                    cellValue = new BigDecimal(o.toString()).setScale(2, BigDecimal.ROUND_HALF_UP).toString();
                } else {
                    cellValue = o.toString();
                }

                cell.setCellValue(cellValue);
            }
            rowIndex++;
        }

//      // 另外一种导出方式
//		HSSFWorkbook workbook = new HSSFWorkbook();
//		HSSFSheet sheet = workbook.createSheet("测试表");
//		HSSFRow titleRow = sheet.createRow(0);
//		sheet.setColumnWidth(titleRow.createCell(0).getColumnIndex(), 256 * 20);
//		titleRow.createCell(0).setCellValue("名称");
//		sheet.setColumnWidth(titleRow.createCell(1).getColumnIndex(), 256 * 20);
//		titleRow.createCell(1).setCellValue("状态(0-已生成，1-待生成)");
//		// 设置应用类型，以及编码
//		response.setContentType("application/msexcel;charset=utf-8");
//		response.setHeader("Content-Disposition", "filename=" + new String("测试表.xls".getBytes("gb2312"), "iso8859-1"));
//		workbook.write(output);
//		output.flush();
//		output.close();

        try {
            workbook.write(os);
            os.flush();// 刷新此输出流并强制将所有缓冲的输出字节写出
            os.close();// 关闭流
            workbook.close();// 释放workbook所占用的所有windows资源
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
