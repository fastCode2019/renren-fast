/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.modules.sys.controller;

import cn.hutool.core.date.DateUtil;
import io.renren.common.annotation.SysLog;
import io.renren.common.utils.Constant;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.validator.Assert;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.redDuck.entity.UserPaymentEntity;
import io.renren.modules.redDuck.entity.UserTaskEntity;
import io.renren.modules.redDuck.service.UserPaymentService;
import io.renren.modules.sys.entity.SysUserEntity;
import io.renren.modules.sys.form.PasswordForm;
import io.renren.modules.sys.service.SysUserRoleService;
import io.renren.modules.sys.service.SysUserService;
import org.apache.commons.lang.ArrayUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 系统用户
 *
 * @author Mark sunlightcs@gmail.com
 */
@RestController
@RequestMapping("/sys/user")
public class SysUserController extends AbstractController {
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysUserRoleService sysUserRoleService;

	@Autowired
	private UserPaymentService userPaymentService;


	/**
	 * 所有用户列表
	 */
	@GetMapping("/list")
	@RequiresPermissions("sys:user:list")
	public R list(@RequestParam Map<String, Object> params){
		//只有超级管理员，才能查看所有管理员列表
		if(getUserId() != Constant.SUPER_ADMIN){
			params.put("createUserId", getUserId());
		}
		PageUtils page = sysUserService.queryPage(params);

		return R.ok().put("page", page);
	}
	
	/**
	 * 获取登录的用户信息
	 */
	@GetMapping("/info")
	public R info(){
		return R.ok().put("user", getUser());
	}
	
	/**
	 * 修改登录用户密码
	 */
	@SysLog("修改密码")
	@PostMapping("/password")
	public R password(@RequestBody PasswordForm form){
		Assert.isBlank(form.getNewPassword(), "新密码不为能空");
		
		//sha256加密
		String password = new Sha256Hash(form.getPassword(), getUser().getSalt()).toHex();
		//sha256加密
		String newPassword = new Sha256Hash(form.getNewPassword(), getUser().getSalt()).toHex();
				
		//更新密码
		boolean flag = sysUserService.updatePassword(getUserId(), password, newPassword);
		if(!flag){
			return R.error("原密码不正确");
		}
		
		return R.ok();
	}
	
	/**
	 * 用户信息
	 */
	@GetMapping("/info/{userId}")
	@RequiresPermissions("sys:user:info")
	public R info(@PathVariable("userId") Long userId){
		SysUserEntity user = sysUserService.getById(userId);
		
		//获取用户所属的角色列表
		List<Long> roleIdList = sysUserRoleService.queryRoleIdList(userId);
		user.setRoleIdList(roleIdList);
		
		return R.ok().put("user", user);
	}
	
	/**
	 * 保存用户
	 */
	@SysLog("保存用户")
	@PostMapping("/save")
	@RequiresPermissions("sys:user:save")
	public R save(@RequestBody SysUserEntity user){
		ValidatorUtils.validateEntity(user, AddGroup.class);
		user.setCreateUserId(getUserId());

		if(getUserId() != 1){
			sysUserService.subAmount(getUserId(), 35000L);
			UserPaymentEntity payment = new UserPaymentEntity();
			payment.setUserId(getUserId().intValue());
			payment.setCreateTime(DateUtil.date());
			payment.setType(0);
			payment.setAmount(35000L);
			payment.setOrderName("开户消费");
			payment.setOrderId(null);
		}

		if(user.getRoleIdList() == null || user.getRoleIdList().size() == 0){
			List<Long> roles = new ArrayList<Long>();
			roles.add(1L);// 添加普通会员
			user.setRoleIdList(roles);
		}
		sysUserService.saveUser(user);
		UserTaskEntity userTask = new UserTaskEntity();
		userTask.setCreateTime(new Date());
		userTask.setType(0);
		userTask.setTaskId(2);
		userTask.setTaskName("定时扫码打开 ");
		userTask.setUserName(user.getUsername());
		userTask.setUserId(user.getUserId().intValue());
		return R.ok();
	}
	
	/**
	 * 修改用户
	 */
	@SysLog("修改用户")
	@PostMapping("/update")
	@RequiresPermissions("sys:user:update")
	public R update(@RequestBody SysUserEntity user){
		ValidatorUtils.validateEntity(user, UpdateGroup.class);
		user.setCreateUserId(getUserId());
		sysUserService.updateAnPassword(user);
		
		return R.ok();
	}

	/**
	 * rechragng
	 */
	@SysLog("修改用户")
	@PostMapping("/recharge/{userId}")
	@RequiresPermissions("sys:user:update")
	public R recharge (@PathVariable("userId") Long userId, @RequestParam Integer amount,@RequestParam Double factor){
		Double zs;
		if (factor != null && factor < 1 && factor >= 0){
		} else {
			factor = 0.00;
		}
		zs = amount * factor;
		UserPaymentEntity payment = new UserPaymentEntity();
		UserPaymentEntity zhensong = new UserPaymentEntity();
		payment.setUserId(userId.intValue());
		payment.setCreateTime(DateUtil.date());
		payment.setType(1); //c
		payment.setAmount(amount.longValue());
		payment.setOrderName("充值订单");
		payment.setOrderId("");
		zhensong.setUserId(userId.intValue());
		zhensong.setCreateTime(DateUtil.date());
		zhensong.setType(4); // 赠送
		zhensong.setAmount(zs.longValue());
		zhensong.setOrderName("充值赠送");
		zhensong.setOrderId("");

		if (getUserId() != 1){
			sysUserService.subAmount(getUserId(), amount + zs.longValue());
		}
		synchronized (this){
			SysUserEntity user = sysUserService.getById(userId);
			payment.setOldAmount(user.getAmount());
			zhensong.setOldAmount(user.getAmount());
			user.setCreateUserId(getUserId());
			user.setAmount(user.getAmount() + amount + zs.longValue());
			sysUserService.updateById(user);
		}
		userPaymentService.save(payment);
		if (zs > 0) {
			userPaymentService.save(zhensong);
		}
		return R.ok();
	}

	/**
	 * 删除用户
	 */
	@SysLog("删除用户")
	@PostMapping("/delete")
	@RequiresPermissions("sys:user:delete")
	public R delete(@RequestBody Long[] userIds){
		if(ArrayUtils.contains(userIds, 1L)){
			return R.error("系统管理员不能删除");
		}
		if(ArrayUtils.contains(userIds, getUserId())){
			return R.error("当前用户不能删除");
		}
		sysUserService.deleteBatch(userIds);
		return R.ok();
	}
}
