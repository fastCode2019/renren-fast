package io.renren.modules.job.task;

import io.renren.modules.redDuck.service.UserOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("CheckOrderStatus")
public class CheckOrderStatus implements ITask {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private UserOrderService userOrderService;

    @Override
    public void run(String params) {
        logger.debug("检查订单状态正在执行，参数为：{}", params);
        userOrderService.checkOrderStatus();
        logger.debug("检查订单状态结束：{}");
    }
}
