package io.renren.modules.job.task;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.modules.redDuck.entity.*;
import io.renren.modules.redDuck.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Component("CheckOrderAmount")
public class CheckOrderAmount implements ITask {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private UserOrderService userOrderService;
    @Autowired
    private UserPaymentService userPaymentService;
    @Autowired
    private OrderTaskService orderTaskService;
    @Autowired
    private OrderLiquidationService orderLiquidationService;
    @Autowired
    private UserOrderRecordService userOrderRecordService;
    @Autowired
    private UserOrderSService userOrderSService;

    @Override
    public void run(String params) {

        logger.debug("统计收益正在执行，参数为：{}", params);
        String now = DateUtil.formatDate(DateUtil.date());
        Date lastDay = DateUtil.parseDate(now);

        List<UserOrderEntity> orderEntities = userOrderService.getBaseMapper().selectList(new QueryWrapper<UserOrderEntity>().le("create_time", lastDay));
        Map<String, UserOrderEntity> orderMap = orderEntities.stream().collect(Collectors.toMap(UserOrderEntity::getOrderId, a -> a, (k, k1)-> k));
        List<OrderTaskEntity> taskEntities = orderTaskService.getBaseMapper().selectList(new QueryWrapper<>());
        Map<String, OrderTaskEntity> taskMap = taskEntities.stream().collect(Collectors.toMap(OrderTaskEntity::getTaskId, a -> a, (k, k1)-> k));
        // 普通消费 和 流量消费
        List<UserPaymentEntity> paymentEntities = userPaymentService.getBaseMapper().selectList(new QueryWrapper<UserPaymentEntity>().in("type","0","3").ne("user_id", "1").isNull("payment_id").le("create_time", lastDay));
        List<UserPaymentEntity> refundEntities = userPaymentService.getBaseMapper().selectList(new QueryWrapper<UserPaymentEntity>().in("type","2").ne("user_id", "1").isNull("payment_id").le("create_time", lastDay));
        List<UserPaymentEntity> chongzhiPayments = userPaymentService.getBaseMapper().selectList(new QueryWrapper<UserPaymentEntity>().eq("type","4").isNull("payment_id").le("create_time", lastDay));
        Double total = 0.0;
        String paymentId = UUID.randomUUID().toString();
        for (UserPaymentEntity pay: paymentEntities) {
            String taskId = null;
            if (pay.getTaskId() == null){
                UserOrderEntity order = orderMap.get(pay.getOrderId());
                if (order != null) {
                    taskId = order.getTaskId();
                }
            }else{
                taskId = pay.getTaskId();
            }
            OrderTaskEntity task = taskMap.get(taskId);
            if (task != null) {
                if(task.getTaskBasePrice() == null){
                  continue;
                }
                Integer cha = task.getTaskPrice() - task.getTaskBasePrice();
                Double count = pay.getAmount().doubleValue() / task.getTaskPrice();
                total += cha * count / 1000.0;
            }else{
                total += pay.getAmount();
            }
            pay.setPaymentId(paymentId);
        }
        Double zhichu = 0.0;
        for (UserPaymentEntity pay: chongzhiPayments) {
            zhichu += pay.getAmount().doubleValue() / 1000;
            pay.setPaymentId(paymentId);
        }
        for (UserPaymentEntity pay: refundEntities) {
            String taskId = null;
            if (pay.getTaskId() == null){
                UserOrderEntity order = orderMap.get(pay.getOrderId());
                if (order != null) {
                    taskId = order.getTaskId();
                }
            }else{
                taskId = pay.getTaskId();
            }
            OrderTaskEntity task = taskMap.get(taskId);
            if (task != null) {
                if(task.getTaskBasePrice() == null){
                    continue;
                }
                Integer cha = task.getTaskPrice() - task.getTaskBasePrice();
                Double count = pay.getAmount().doubleValue() / task.getTaskPrice();
                zhichu += cha * count / 1000.0;
            }
            pay.setPaymentId(paymentId);
        }
        if (zhichu != 0 || total != 0) {
           OrderLiquidationEntity liq = new OrderLiquidationEntity();
           liq.setCreateTime(new Date());
           liq.setPaymentId(paymentId);
           liq.setIncome(total);
           liq.setExpenses(zhichu);
           liq.setProfit(total - zhichu);
           liq.setName(DateUtil.formatDate(lastDay));
           orderLiquidationService.save(liq);
           logger.debug("生成收益账单数据..............");
       }

        userPaymentService.updateBatchById(paymentEntities);
        userPaymentService.updateBatchById(chongzhiPayments);
        userPaymentService.updateBatchById(refundEntities);

        Date day7 = DateUtil.offsetDay(DateUtil.parseDate(now), -6);
        logger.debug("-----七天后->" + day7);
        logger.debug("正在删除七天后的账单数据..............");
        userPaymentService.remove(new QueryWrapper<UserPaymentEntity>().le("create_time", day7));
        List<UserOrderEntity> deletes = userOrderService.getBaseMapper().selectList(new QueryWrapper<UserOrderEntity>().eq("order_status","1").le("create_time", day7));
        List<String> orderIds = deletes.stream().map(d -> d.getOrderId()).collect(Collectors.toList());
        logger.debug("正在删除七天后的订单数据................");
        userOrderService.tryBatchDeleteRedYaOrder(orderIds);
        userOrderService.remove(new QueryWrapper<UserOrderEntity>().in("order_status","1", "3").le("create_time", day7));
        userOrderRecordService.remove(new QueryWrapper<UserOrderRecordEntity>().le("create_time", day7));
        userOrderSService.remove(new QueryWrapper<UserOrderSEntity>().in("order_status","2", "3").le("create_time", day7));
        logger.debug("---------------统计收益完成------------------------");
    }
}
