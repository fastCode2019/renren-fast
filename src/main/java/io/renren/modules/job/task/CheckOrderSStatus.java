package io.renren.modules.job.task;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.modules.redDuck.entity.UserOrderSEntity;
import io.renren.modules.redDuck.entity.UserPaymentEntity;
import io.renren.modules.redDuck.service.UserOrderSService;
import io.renren.modules.redDuck.service.UserOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component("StepOrderByDay")
public class CheckOrderSStatus implements ITask {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private UserOrderSService userOrderService;

    @Override
    public void run(String params) {

        logger.debug("分步处理订单正在执行，参数为：{}", params);
        String now = DateUtil.formatDate(DateUtil.date());
        List<UserOrderSEntity> orders = userOrderService.getBaseMapper().selectList(new QueryWrapper<UserOrderSEntity>().eq("order_status", 1));
        for (UserOrderSEntity order: orders) {
            Date ud = order.getUpdateTime();
            try{
                if(ud != null && DateUtil.formatDate(ud) != now){
                    userOrderService.stepOrder(order);
                }else if(ud == null){
                    userOrderService.stepOrder(order);
                }
            }catch ( Exception e ){
                logger.error(order.getOrderName() + "处理失败" + e.getMessage());
            }
        }
        logger.debug("分步处理订单结束", params);

    }
}
