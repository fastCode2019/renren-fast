package io.renren.modules.job.task;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.modules.redDuck.entity.UserOrderEntity;
import io.renren.modules.redDuck.service.UserOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("OrderRedYa")
public class OrderRedYa implements ITask {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private UserOrderService userOrderService;

    @Override
    public void run(String params) {

        logger.debug("红鸭下单正在执行，参数为：{}", params);
        List<UserOrderEntity> orders = userOrderService.getBaseMapper().selectList(new QueryWrapper<UserOrderEntity>().eq("order_status", "-1"));
        for (UserOrderEntity order: orders) {
            try{
                userOrderService.toRedYa(order);
            }catch ( Exception e ){
                logger.error(order.getOrderName() + "处理失败" + e.getMessage());
            }
        }
        logger.debug("红鸭下单结束", params);

    }
}
