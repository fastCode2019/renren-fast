package io.renren.modules.redDuck.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-03 09:49:53
 */
@Data
@TableName("order_liquidation")
public class OrderLiquidationEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private String name;
	/**
	 * 支出
	 */
	private Double expenses;
	/**
	 * 收入
	 */
	private Double income;
	/**
	 * 利润
	 */
	private Double profit;
	/**
	 * 
	 */
	private String paymentId;
	/**
	 * 创建时间
	 */
	private Date createTime;

}
