package io.renren.modules.redDuck.entity;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-11-06 18:04:49
 */
@Data
@TableName("user_order")
public class UserOrderEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private Integer userId;
	/**
	 * 
	 */
	private String orderId;
	/**
	 * 
	 */
	private String orderName;
	/**
	 * 
	 */
	private String orderStatus;
	/**
	 * 
	 */
	private String taskId;
	/**
	 * 
	 */
	private String taskType;
	/**
	 * 
	 */
	private Integer orderPrice;
	/**
	 * 
	 */
	private Date createTime;
	/**
	 * 
	 */
	private Date updateTime;
	/**
	 *
	 */
	private Integer realNum;
	/**
	 *
	 */
	private Integer maxNum;

	@TableField(exist = false)
	private JSONObject orderDetail;

	private Integer orderSId;

	private String orderParam;

	private String redyaDetail;

}
