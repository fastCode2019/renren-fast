package io.renren.modules.redDuck.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-11-25 01:46:21
 */
@Data
@TableName("sys_notice")
public class SysNoticeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 内容
	 */
	private String content;
	/**
	 * 标题
	 */
	private String title;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 禁用
	 */
	private Integer disable;
	/**
	 * 创建时间
	 */
	private Date createTime;

}
