package io.renren.modules.redDuck.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-11-25 01:46:21
 */
@Data
@TableName("user_order_s")
public class UserOrderSEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 用户id
	 */
	private Integer userId;
	/**
	 * 用户id
	 */
	private String orderId;
	/**
	 * 订单名
	 */
	private String orderName;
	/**
	 * 价格
	 */
	private Integer orderPrice;
	/**
	 * 订单数量
	 */
	private Integer orderNum;
	/**
	 * 图片
	 */
	private String orderImage;
	/**
	 * 订单状态
	 */
	private Integer orderStatus;
	/**
	 * 订单状态
	 */
	private String taskId;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 
	 */
	private Date updateTime;

	private Integer totalDay;
	private Integer useDay;
	private Integer useNum;

}
