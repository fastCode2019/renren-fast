package io.renren.modules.redDuck.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-11-06 18:04:49
 */
@Data
@TableName("order_task")
public class OrderTaskEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private String taskId;
	/**
	 * 
	 */
	private String taskType;
	/**
	 * 
	 */
	private String taskName;

	/**
	 * 
	 */
	private Integer taskPrice;

	/**
	 *
	 */
	private Integer taskBasePrice;
	/**
	 *
	 */
	private String taskFieldJson;

	private String taskCreateApi;
	private String taskDeleteApi;
	private String taskUpdateApi;
	private String taskActionApi;

	/**
	 * 
	 */
	private Date createTime;
	/**
	 * 
	 */
	private Date updateTime;

}
