package io.renren.modules.redDuck.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-11-11 21:31:37
 */
@Data
@TableName("user_payment")
public class UserPaymentEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private Integer userId;
	/**
	 * 用户订单
	 */
	private Integer userOrderId;
	/**
	 * 订单名
	 */
	private String orderName;
	/**
	 * 账单金额
	 */
	private Long amount;
	/**
	 * 账单类型
	 */
	private Integer type;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 账单id
	 */
	private String paymentId;
	/**
	 * 订单id
	 */
	private String orderId;

	private String taskId;

	private Long oldAmount;
}
