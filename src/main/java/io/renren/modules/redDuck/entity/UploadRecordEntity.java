package io.renren.modules.redDuck.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-11-19 23:10:03
 */
@Data
@TableName("upload_record")
public class UploadRecordEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private Integer userId;
	/**
	 * 总任务数
	 */
	private Integer totalNum;
	/**
	 * 完成任务数
	 */
	private Integer finishNum;
	/**
	 * 失败原因
	 */
	private String failures;
	/**
	 * 上传时间
	 */
	private Date createTime;

}
