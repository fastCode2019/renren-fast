package io.renren.modules.redDuck.entity;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@Data
public class OrderFrom{
    Map<String, String> params;
    List<MultipartFile> files;
}

