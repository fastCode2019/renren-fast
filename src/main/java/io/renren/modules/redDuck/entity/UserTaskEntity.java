package io.renren.modules.redDuck.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-11-25 01:46:22
 */
@Data
@TableName("user_task")
public class UserTaskEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 用户id
	 */
	private Integer userId;
	/**
	 * 任务Id
	 */
	private Integer taskId;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 任务名称
	 */
	private String taskName;
	/**
	 * 用户名称
	 */
	private String userName;

	private Integer type;
}
