package io.renren.modules.redDuck.controller;

import java.io.IOException;
import java.util.*;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.exception.RRException;
import io.renren.common.utils.*;
import io.renren.modules.redDuck.entity.*;
import io.renren.modules.redDuck.service.OrderTaskService;
import io.renren.modules.redDuck.service.UploadRecordService;
import io.renren.modules.redDuck.service.UserPaymentService;
import io.renren.modules.sys.controller.AbstractController;
import io.renren.modules.sys.entity.SysUserEntity;
import io.renren.modules.sys.service.SysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.renren.modules.redDuck.service.UserOrderService;
import org.springframework.web.multipart.MultipartFile;


/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-11-06 18:04:49
 */
@RestController
@RequestMapping("redDuck/userorder")
public class UserOrderController extends AbstractController  {
    @Autowired
    private UserOrderService userOrderService;
    @Autowired
    private UserPaymentService userPaymentService;
    @Autowired
    private OrderTaskService orderTaskService;
    @Autowired
    private UploadRecordService uploadRecordService;
    @Autowired
    private SysUserService sysUserService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("redDuck:userorder:list")
    public R list(@RequestParam Map<String, Object> params){
        if (getUserId() != 1){
            params.put("userId", getUserId());
        }
        PageUtils page = userOrderService.queryPage(params);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("redDuck:userorder:info")
    public R info(@PathVariable("id") Integer id){
		UserOrderEntity userOrder = userOrderService.getById(id);
        return R.ok().put("userOrder", userOrder);
    }



    @RequestMapping("/orderInfo/{id}")
    @RequiresPermissions("redDuck:userorder:info")
    public R oderInfo(@PathVariable("id") String id)  {
        return R.ok(userOrderService.orderDetail(id));
    }

    /**
     * 下单
     */
    @RequestMapping("/order")
    @RequiresPermissions("redDuck:userorder:save")
    public R order(@RequestParam Map<String, String> params)  {
        List<MultipartFile> files = new ArrayList<>();
        if(params.containsKey("advertInfoUrl")){
            try{
                MultipartFile file = HttpUtils.down(params.get("advertInfoUrl"));
                if (file != null ){
                    files = new ArrayList<>();
                    files.add(0, file);
                    params.put("QEcodeImg", "0");
                }
            }catch (IOException e){
                throw new RRException("url错误");
            }
        }
        UserOrderRecordEntity record = userOrderService.orderRedYa(getUserId(), ApiParamFix.FixRadYaParam(params, true), files);
        if(record.getReason() != null){
            return R.error(record.getReason());
        }
        return R.ok(record.getRedYaId());
    }

    /**
     * 下单
     */
    @PostMapping("/muti-order")
    @RequiresPermissions("redDuck:userorder:save")
    public R order(@RequestParam Map<String, String> params, @RequestParam("files") List<MultipartFile> files)  {
        if(params.containsKey("advertInfoUrl")){
            try{
                MultipartFile file = HttpUtils.down(params.get("advertInfoUrl"));
                if (file != null ){
                    files = new ArrayList<>();
                    files.add(0, file);
                    params.put("QEcodeImg", "0");
                }
            }catch (IOException e){
                throw new RRException("url错误");
            }
        }
        UserOrderRecordEntity record = userOrderService.orderRedYa(getUserId(), ApiParamFix.FixRadYaParam(params, true), files);
        if(record.getReason() != null){
            return R.error(record.getReason());
        }
        return R.ok(record.getRedYaId());
    }


    /**
     * 保存
     */
    @RequestMapping("/unified-order")
    @RequiresPermissions("redDuck:userorder:save")
    public R save(@RequestBody Map<String, String> params)  {
        UserOrderRecordEntity record = userOrderService.unifiedOrder(getUserId(), params);
        if(record.getReason() != null){
            return R.error(record.getReason());
        }
        return R.ok(record.getRedYaId());
    }

    /**
     * 保存
     */
    @RequestMapping("/refund")
    @RequiresPermissions("redDuck:userorder:refund")
    public R refund(@RequestBody Map<String, String> params)  {
        UserOrderEntity order = userOrderService.getById(params.get("id"));
        if (order.getOrderStatus().equals("1")) {
            UserPaymentEntity payment = new UserPaymentEntity();
            Long amount = Long.parseLong(params.get("amount"));
            if (amount > order.getOrderPrice()){
                throw new RRException("不能大于订单价格");
            }
            synchronized (this){
                SysUserEntity user = sysUserService.getById(order.getUserId());
                payment.setOldAmount(user.getAmount());
                user.setAmount(user.getAmount() + amount);
                sysUserService.updateById(user);
            }
            payment.setUserId(order.getUserId());
            payment.setCreateTime(new Date());
            payment.setType(2);
            payment.setAmount(amount.longValue());
            payment.setOrderName(order.getOrderName());
            payment.setUserOrderId(order.getId());
            payment.setOrderId(order.getOrderId());
            userPaymentService.save(payment);
            order.setOrderStatus("3");
            userOrderService.updateById(order);
            return R.ok();
        }else{
           throw new RRException("订单未下线");
        }
    }

    /**
     * 保存
     */
    @RequestMapping("/offline-refund")
    @RequiresPermissions("redDuck:userorder:update")
    public R orderForceOffline(@RequestBody Map<String, String> params)  {
        UserOrderEntity order = userOrderService.getById(params.get("id"));
        String orderId = userOrderService.fetchOrderId(order.getOrderName());

        if (order.getOrderStatus().equals("0") && order.getOrderId() == null) {
            if (orderId != null) {
                userOrderService.orderOffLine(orderId, order.getUserId());
            }
            UserPaymentEntity payment = new UserPaymentEntity();
            SysUserEntity user = sysUserService.getById(order.getUserId());
            payment.setOldAmount(user.getAmount());
            sysUserService.subAmount(order.getUserId().longValue(), -order.getOrderPrice().longValue());
            payment.setUserId(order.getUserId());
            payment.setCreateTime(new Date());
            payment.setType(2);
            payment.setAmount(order.getOrderPrice().longValue());
            payment.setOrderName(order.getOrderName());
            payment.setUserOrderId(order.getId());
            payment.setOrderId(order.getOrderId());
            userPaymentService.save(payment);
            order.setOrderStatus("1");
            order.setUpdateTime(new Date());
            userOrderService.updateById(order);
            return R.ok();
        }else{
            throw new RRException("没有权限操作");
        }
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    @RequiresPermissions("redDuck:userorder:update")
    public R update(@RequestParam("files") List<MultipartFile> files, @RequestParam Map<String, String> params){
        userOrderService.orderUpdate(params.get("advertInfoId"), params, files);
        return R.ok();
    }

    @PostMapping("/upload")
    @RequiresPermissions("redDuck:userorder:save")
    public R upload(@RequestParam("xls") MultipartFile file, @RequestParam Map<String, String> params) throws Exception {
        String taskId = params.get("attribusteBaseId");
        if(taskId == null){
            throw new RRException("任务Id为空");
        }
        List<OrderFrom> list = ExcelUtil.importExcel(file);

        List<String> error = new ArrayList<>();
        Map<String, String> taskMap = new HashMap<>();
        for (OrderFrom of: list) {
            try{
                Map<String, String> param = of.getParams();
                taskMap = ApiParamFix.FixRadYaParam(param, false);
                taskMap.put("attribusteBaseId", taskId);
                userOrderService.orderRedYa(getUserId(), taskMap , of.getFiles());
            }catch (Exception e){
                error.add(taskMap.get("advertInfoTitle") +" :" + e.getMessage());
            }
        }
        UploadRecordEntity record = new UploadRecordEntity();
        record.setUserId(getUserId().intValue());
        record.setCreateTime(new Date());
        record.setTotalNum(list.size());
        record.setFinishNum(list.size() - error.size());
        record.setFailures(JSONObject.toJSON(error).toString());
        uploadRecordService.save(record);
        if(error.isEmpty()){
            return R.ok();
        }else{
            return R.error(JSONObject.toJSON(error).toString());
        }
    }

    /**
     * 保存
     */
    @RequestMapping("/offline")
    @RequiresPermissions("redDuck:userorder:update")
    public R offline(@RequestBody String[] ids)  {
        for (String id: ids) {
            JSONObject json = userOrderService.orderDetail(id);
            if(json == null){
                continue;
            }
            UserOrderEntity order = userOrderService.getOne(new QueryWrapper<UserOrderEntity>().eq("order_id", id));
            if(order.getOrderStatus().equals("0")){
                order.setOrderStatus("2"); // 处理中
                userOrderService.updateById(order);
            }
            // 红鸭下线
            Integer status = json.getInteger("advertInfoIsdelete");
            if (status == 0) {
                userOrderService.orderOffLine(id, getUserId().intValue());
            }
        }
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("redDuck:userorder:delete")
    public R delete(@RequestBody String[] ids){
        for (String id: ids) {
            JSONObject json = userOrderService.orderDetail(id);
            if(json == null){
                userOrderService.remove(new QueryWrapper<UserOrderEntity>().eq("order_id", id));
                continue;
            }
            UserOrderEntity order = userOrderService.getOne(new QueryWrapper<UserOrderEntity>().eq("order_id", id));
            if(!order.getOrderStatus().equals("1")){
               throw new RRException("订单未下线");
            }

            Integer status = json.getInteger("advertInfoIsdelete");
            if(status == 1){
                userOrderService.remove(new QueryWrapper<UserOrderEntity>().eq("order_id", id));
                userOrderService.orderDelete(id);
            }
        }
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/deleteById")
    @RequiresPermissions("redDuck:userorder:delete")
    public R deleteById(@RequestBody Integer[] ids){
        for (Integer id: ids) {
            userOrderService.removeById(id);
        }
        return R.ok();
    }

}
