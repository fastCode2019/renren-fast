package io.renren.modules.redDuck.controller;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.exception.RRException;
import io.renren.common.utils.*;
import io.renren.modules.redDuck.entity.UserTaskEntity;
import io.renren.modules.redDuck.service.UserTaskService;
import io.renren.modules.sys.controller.AbstractController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.redDuck.entity.OrderTaskEntity;
import io.renren.modules.redDuck.service.OrderTaskService;

import static io.renren.common.utils.AES.mapSortToString;


/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-11-06 18:04:49
 */
@RestController
@RequestMapping("redDuck/ordertask")
public class OrderTaskController extends AbstractController {
    @Autowired
    private OrderTaskService orderTaskService;
    @Autowired
    private UserTaskService userTaskService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("redDuck:ordertask:list")
    public R list(@RequestParam Map<String, Object> params){
        if(getUserId() != 1){ //admin
            List<UserTaskEntity> userTasks = userTaskService.list(new QueryWrapper<UserTaskEntity>().eq("user_id", getUserId()).eq("type", 0));
            params.put("taskIds", userTasks.stream().map(UserTaskEntity::getTaskId).collect(Collectors.toList()));
        }
        PageUtils page = orderTaskService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 列表
     */
    @RequestMapping("/all")
    @RequiresPermissions("redDuck:ordertask:list")
    public R allList(@RequestParam Map<String, Object> params){
        PageUtils page = orderTaskService.queryPage(params);
        return R.ok().put("page", page);
    }

    /**
     * 流量主
     */
    @RequestMapping("/package")
    @RequiresPermissions("redDuck:ordertask:list")
    public R type(@RequestParam Map<String, Object> params){
        List<UserTaskEntity> userTasks = userTaskService.list(new QueryWrapper<UserTaskEntity>().eq("user_id", getUserId()).eq("type", 1));
        if(userTasks.size() < 1){
            throw new RRException("没有流量主权限");
        }
        OrderTaskEntity task = orderTaskService.getById(userTasks.get(0).getTaskId());
        return R.ok().put("orderTask", task);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("redDuck:ordertask:info")
    public R info(@PathVariable("id") Integer id){
		OrderTaskEntity orderTask = orderTaskService.getById(id);
        return R.ok().put("orderTask", orderTask);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("redDuck:ordertask:save")
    public R save(@RequestBody OrderTaskEntity orderTask){
		orderTaskService.save(orderTask);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("redDuck:ordertask:update")
    public R update(@RequestBody OrderTaskEntity orderTask){
		orderTaskService.updateById(orderTask);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("redDuck:ordertask:delete")
    public R delete(@RequestBody Integer[] ids){
		orderTaskService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

    /**
     * 信息
     */
    @RequestMapping("/getDyUserId")
    @RequiresPermissions("redDuck:ordertask:info")
    public JSONObject getDyUserId(@RequestParam Map<String, String> param){
        Map<String, String> map = new HashMap<>();
        map.put("url",param.get("url"));
        JSONObject json = RedYaApi.post("https://api.redya.cc/api/advert/static/getDyUserId", map);
        return json;
    }


}
