package io.renren.modules.redDuck.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.renren.modules.redDuck.entity.SysNoticeEntity;
import io.renren.modules.redDuck.service.SysNoticeService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-11-25 01:46:21
 */
@RestController
@RequestMapping("redDuck/sysnotice")
public class SysNoticeController {
    @Autowired
    private SysNoticeService sysNoticeService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("redDuck:sysnotice:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysNoticeService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 列表
     */
    @GetMapping("/config.jsp")
    public String config(){
            JSONObject json = new JSONObject();
           json.put("imageUrl", "http://8.210.237.191:8080/renren-fast/sys/oss/upload?token=b7fa553c42f7eb7bc8f1334c9a071c20");
           json.put("imagePath", "/ueditor/image/");
           json.put("imageFieldName", "upfile");
           json.put("imageMaxSize", 2048);

        return "111";
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("redDuck:sysnotice:info")
    public R info(@PathVariable("id") Integer id){
		SysNoticeEntity sysNotice = sysNoticeService.getById(id);

        return R.ok().put("sysNotice", sysNotice);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("redDuck:sysnotice:save")
    public R save(@RequestBody SysNoticeEntity sysNotice){
		sysNoticeService.save(sysNotice);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("redDuck:sysnotice:update")
    public R update(@RequestBody SysNoticeEntity sysNotice){
		sysNoticeService.updateById(sysNotice);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("redDuck:sysnotice:delete")
    public R delete(@RequestBody Integer[] ids){
		sysNoticeService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
