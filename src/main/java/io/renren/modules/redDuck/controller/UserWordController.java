package io.renren.modules.redDuck.controller;

import java.util.*;

import com.alibaba.fastjson.JSONObject;
import io.renren.common.exception.RRException;
import io.renren.common.utils.RedYaApi;
import io.renren.modules.sys.controller.AbstractController;
import io.renren.modules.sys.service.SysConfigService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.redDuck.entity.UserWordEntity;
import io.renren.modules.redDuck.service.UserWordService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-03 09:29:25
 */
@RestController
@RequestMapping("redDuck/userword")
public class UserWordController extends AbstractController {
    @Autowired
    private UserWordService userWordService;
    @Autowired
    private SysConfigService sysConfigService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("redDuck:userword:list")
    public R list(@RequestParam Map<String, Object> params){
        Map<String, String> map = new HashMap<>();
        map.put("page", "1");
        map.put("limit", "10");
        map.put("userId", "b6700d8bca3b42a8808a34f2410bc562");
        JSONObject  json = RedYaApi.post("https://api.redya.cc/api/allot/lexiconBase/selectListAll", map);
        logger.error(json.toString());
        PageUtils page = userWordService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("redDuck:userword:info")
    public R info(@PathVariable("id") String id){
		UserWordEntity userWord = userWordService.getById(id);

        return R.ok().put("userWord", userWord);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("redDuck:userword:save")
    public R save(@RequestBody UserWordEntity userWord){
        Map<String, String> map = new HashMap<>();
        map.put("content", userWord.getContent());
        String appId = sysConfigService.getValue("appId");
        map.put("userId", appId);
        JSONObject json = RedYaApi.post("https://api.redya.cc/api/allot/lexiconBase/create", map);
        if(json.getInteger("code") == 0){
            userWord.setStatus(0);
            userWord.setCreateTime(new Date());
            userWord.setUserId(getUserId().intValue());
        }else{
            throw new RRException(json.getString("msg"));
        }
		userWordService.save(userWord);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("redDuck:userword:update")
    public R update(@RequestBody UserWordEntity userWord){
		userWordService.updateById(userWord);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("redDuck:userword:delete")
    public R delete(@RequestBody String[] ids){

        Map<String, String> map = new HashMap<>();
        if(ids.length == 0)
            return R.ok();
        String idString = "";
        for (String id: ids) {
            idString += ("-" + id);
        }
        idString.substring(0, idString.length() -1);
        map.put("id", idString);

        JSONObject json = RedYaApi.post("https://api.redya.cc/api/advert/taskinfo/deleteTaskInfo", map);
        logger.debug(json.toString());
		userWordService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
