package io.renren.modules.redDuck.controller;

import java.util.Arrays;
import java.util.Map;

import io.renren.modules.sys.controller.AbstractController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.redDuck.entity.UserPaymentEntity;
import io.renren.modules.redDuck.service.UserPaymentService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-11-11 21:31:37
 */
@RestController
@RequestMapping("redDuck/userpayment")
public class UserPaymentController extends AbstractController {
    @Autowired
    private UserPaymentService userPaymentService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("redDuck:userpayment:list")
    public R list(@RequestParam Map<String, Object> params){
        if (getUserId() != 1){
            params.put("userId", getUserId());
        }
        PageUtils page = userPaymentService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("redDuck:userpayment:info")
    public R info(@PathVariable("id") Integer id){
		UserPaymentEntity userPayment = userPaymentService.getById(id);

        return R.ok().put("userPayment", userPayment);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("redDuck:userpayment:save")
    public R save(@RequestBody UserPaymentEntity userPayment){
		userPaymentService.save(userPayment);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("redDuck:userpayment:update")
    public R update(@RequestBody UserPaymentEntity userPayment){
		userPaymentService.updateById(userPayment);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("redDuck:userpayment:delete")
    public R delete(@RequestBody Integer[] ids){
		userPaymentService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
