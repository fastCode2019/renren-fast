package io.renren.modules.redDuck.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.redDuck.entity.OrderLiquidationEntity;
import io.renren.modules.redDuck.service.OrderLiquidationService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-03 09:49:53
 */
@RestController
@RequestMapping("redDuck/orderliquidation")
public class OrderLiquidationController {
    @Autowired
    private OrderLiquidationService orderLiquidationService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("redDuck:orderliquidation:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = orderLiquidationService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("redDuck:orderliquidation:info")
    public R info(@PathVariable("id") Integer id){
		OrderLiquidationEntity orderLiquidation = orderLiquidationService.getById(id);

        return R.ok().put("orderLiquidation", orderLiquidation);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("redDuck:orderliquidation:save")
    public R save(@RequestBody OrderLiquidationEntity orderLiquidation){
		orderLiquidationService.save(orderLiquidation);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("redDuck:orderliquidation:update")
    public R update(@RequestBody OrderLiquidationEntity orderLiquidation){
		orderLiquidationService.updateById(orderLiquidation);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("redDuck:orderliquidation:delete")
    public R delete(@RequestBody Integer[] ids){
		orderLiquidationService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
