package io.renren.modules.redDuck.controller;

import java.io.*;
import java.util.*;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.exception.RRException;
import io.renren.common.utils.*;
import io.renren.modules.redDuck.entity.*;
import io.renren.modules.redDuck.service.OrderTaskService;
import io.renren.modules.redDuck.service.UserOrderService;
import io.renren.modules.redDuck.service.UserPaymentService;
import io.renren.modules.sys.controller.AbstractController;
import io.renren.modules.sys.entity.SysUserEntity;
import io.renren.modules.sys.service.SysUserService;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.redDuck.service.UserOrderSService;

import javax.servlet.http.HttpServletResponse;


/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-11-25 01:46:21
 */
@RestController
@RequestMapping("redDuck/userorders")
public class UserOrderSController extends AbstractController {
    @Autowired
    private UserOrderSService userOrderSService;
    @Autowired
    private UserPaymentService userPaymentService;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private OrderTaskService orderTaskService;


    @Autowired
    private UserOrderService userOrderService;
    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("redDuck:userorders:list")
    public R list(@RequestParam Map<String, Object> params){
        if (getUserId() != 1){
            params.put("userId", getUserId());
        }
        PageUtils page = userOrderSService.queryPage(params);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("redDuck:userorders:info")
    public R info(@PathVariable("id") Integer id){
		UserOrderSEntity userOrderS = userOrderSService.getById(id);
        return R.ok().put("userOrderS", userOrderS);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @Transactional
    @RequiresPermissions("redDuck:userorders:save")
    public R save(@RequestBody UserOrderSEntity userOrderS){

        if(userOrderS.getTaskId() == null){
            throw new RRException("没有流量主权限");
        }
        OrderTaskEntity task = orderTaskService.getOne(new QueryWrapper<OrderTaskEntity>()
                .eq(StringUtils.isNotBlank(userOrderS.getTaskId()),"task_id", userOrderS.getTaskId()));
        if (task == null && !task.getTaskType().equals("流量主")){
            throw new RRException("没有流量主权限");
        }
        userOrderS.setCreateTime(new Date());
        userOrderS.setOrderId(UUID.randomUUID().toString());
        userOrderS.setUseNum(0);
        userOrderS.setUseDay(0);
        userOrderS.setUserId(getUserId().intValue());
        if (userOrderS.getOrderNum() == null){
            userOrderS.setOrderNum(1500);
        }
        userOrderS.setOrderStatus(1);
        UserPaymentEntity userPayment = new UserPaymentEntity();
        userPayment.setUserId(getUserId().intValue());
        userPayment.setType(3); // 流量主
        userPayment.setOrderId(userOrderS.getOrderId());
        userPayment.setOrderName(userOrderS.getOrderName());
        userPayment.setAmount(task.getTaskPrice().longValue());
        userPayment.setCreateTime(new Date());
        sysUserService.subAmount(getUserId(), userOrderS.getOrderPrice().longValue());
		userOrderSService.save(userOrderS);
        userPaymentService.save(userPayment);
        return R.ok();
    }

    @RequestMapping("/download")
    @RequiresPermissions("redDuck:userorders:download")
    public void downLoad(@RequestParam Map<String, String> params, HttpServletResponse response){
        String userId = params.get("userId");
        if(userId == null)
            throw new RRException("缺少用户ID");
        List<UserOrderSEntity> orders = userOrderSService.getBaseMapper().selectList(new QueryWrapper<UserOrderSEntity>().eq("user_id", userId).eq("order_status",1));
        List<JSONObject> objects = new ArrayList<>();
        LinkedHashMap<String, String> titleMap = new LinkedHashMap<>();
        // title1设置标题，key固定
        titleMap.put("title1", "积分明细");
        for (UserOrderSEntity order: orders) {
            JSONObject result = new JSONObject();
            result.put("ercode", order.getOrderImage());
            result.put("name", order.getOrderName());
            result.put("type", "定时");
            result.put("time", 1);
            result.put("num", order.getOrderNum());
            objects.add(result);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Content-Disposition", String.format("attachment; filename=\"%s\"", "download.xls"));
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        ArrayList<LinkedHashMap> titleList = new ArrayList<>();
        LinkedHashMap<String, String> headMap = new LinkedHashMap<>();
        headMap.put("ercode", "二维码");
        headMap.put("name", "关注名称");
        headMap.put("type", "任务类型");
        headMap.put("time", "定时(小时)");
        headMap.put("num", "数量");
        titleList.add(headMap);
        JSONArray objectsList = JSONArray.parseArray(objects.toString());
        try {
            OutputStream output = response.getOutputStream();
            response.setContentType("application/msexcel;charset=utf-8");
            response.setHeader("Content-Disposition",
                    "filename=" + new String((getUser().getUsername() + "_流量主.xls").getBytes("gb2312"), "iso8859-1"));
            ExcelUtil.exportExcel(titleList, objectsList, output);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("redDuck:userorders:update")
    public R update(@RequestBody UserOrderSEntity userOrderS){
		userOrderSService.updateById(userOrderS);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("redDuck:userorders:delete")
    public R delete(@RequestBody Integer[] ids){
        for (Integer i: ids) {
            UserOrderSEntity order = userOrderSService.getById(i);
            if(order.getOrderStatus() == 1){
                throw new RRException("不能删除处理中的订单");
            }
        }
		userOrderSService.removeByIds(Arrays.asList(ids));
        return R.ok();
    }
    /**
     * 保存
     */
    @RequestMapping("/finish")
    @RequiresPermissions("redDuck:userorders:finish")
    public R finish(@RequestBody Integer[] ids){
        for (Integer i: ids) {
            synchronized (this){
                UserOrderSEntity order = userOrderSService.getById(i);
                if (order == null) {
                    continue;
                }
                userOrderSService.stepOrder(order);
            }
        }
        return R.ok();
    }



    /**
     * 保存
     */
    @RequestMapping("/failure")
    @Transactional
    @RequiresPermissions("redDuck:userorders:failure")
    public R failure(@RequestBody Integer[] ids){
        for (Integer i: ids) {
            UserOrderSEntity order = userOrderSService.getById(i);
            if(order.getUseDay() != null && order.getUseDay() >= 0){
               throw new RRException("订单进行中，不能退款");
            }

            order.setOrderStatus(3);
            order.setUpdateTime(new Date());
            SysUserEntity user = sysUserService.getById(getUserId());
            sysUserService.subAmount(getUserId(), -order.getOrderPrice().longValue());
            UserPaymentEntity userPayment = new UserPaymentEntity();
            userPayment.setUserId(getUserId().intValue());
            userPayment.setType(2); // 退款
            userPayment.setOldAmount(user.getAmount());
            userPayment.setOrderId(order.getOrderId());
            userPayment.setOrderName(order.getOrderName());
            userPayment.setAmount(order.getOrderPrice().longValue());
            userPayment.setCreateTime(new Date());
            userPaymentService.save(userPayment);
            userOrderSService.updateById(order);
        }
        return R.ok();
    }
}
