package io.renren.modules.redDuck.controller;

import java.util.Arrays;
import java.util.Map;

import io.renren.modules.sys.controller.AbstractController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.redDuck.entity.UploadRecordEntity;
import io.renren.modules.redDuck.service.UploadRecordService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-11-19 23:10:03
 */
@RestController
@RequestMapping("redDuck/uploadrecord")
public class UploadRecordController extends AbstractController {
    @Autowired
    private UploadRecordService uploadRecordService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("redDuck:uploadrecord:list")
    public R list(@RequestParam Map<String, Object> params){
        params.put("userId", getUserId());
        PageUtils page = uploadRecordService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("redDuck:uploadrecord:info")
    public R info(@PathVariable("id") Integer id){
		UploadRecordEntity uploadRecord = uploadRecordService.getById(id);

        return R.ok().put("uploadRecord", uploadRecord);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("redDuck:uploadrecord:save")
    public R save(@RequestBody UploadRecordEntity uploadRecord){
		uploadRecordService.save(uploadRecord);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("redDuck:uploadrecord:update")
    public R update(@RequestBody UploadRecordEntity uploadRecord){
		uploadRecordService.updateById(uploadRecord);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("redDuck:uploadrecord:delete")
    public R delete(@RequestBody Integer[] ids){
		uploadRecordService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
