package io.renren.modules.redDuck.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.exception.RRException;
import io.renren.common.utils.ApiParamFix;
import io.renren.common.utils.HttpUtils;
import io.renren.modules.redDuck.entity.OrderTaskEntity;
import io.renren.modules.redDuck.entity.UserOrderEntity;
import io.renren.modules.redDuck.service.OrderTaskService;
import io.renren.modules.redDuck.service.UserOrderService;
import io.renren.modules.sys.controller.AbstractController;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.redDuck.entity.UserOrderRecordEntity;
import io.renren.modules.redDuck.service.UserOrderRecordService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import org.springframework.web.multipart.MultipartFile;


/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-08 21:53:52
 */
@RestController
@RequestMapping("redDuck/userorderrecord")
public class UserOrderRecordController extends AbstractController {
    @Autowired
    private UserOrderRecordService userOrderRecordService;
    @Autowired
    private UserOrderService userOrderService;

    @Autowired
    private OrderTaskService orderTaskService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("redDuck:userorderrecord:list")
    public R list(@RequestParam Map<String, Object> params){
        if (getUserId() != 1){
            params.put("userId", getUserId());
        }
        PageUtils page = userOrderRecordService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("redDuck:userorderrecord:info")
    public R info(@PathVariable("id") Integer id){
		UserOrderRecordEntity userOrderRecord = userOrderRecordService.getById(id);
        return R.ok().put("userOrderRecord", userOrderRecord);
    }

    /**
     * 信息
     */
    @RequestMapping("/checkOrder/{id}")
    @RequiresPermissions("redDuck:userorderrecord:info")
    public R checkOrder(@PathVariable("id") Integer id){
        UserOrderRecordEntity userOrderRecord = userOrderRecordService.getById(id);
        if (userOrderRecord.getOrderId() != null) {
           UserOrderEntity order = userOrderService.getById(userOrderRecord.getOrderId());
           if (order != null && order.getOrderId() == null) {
               String orderId = userOrderService.fetchOrderId(order.getOrderName());
               order.setOrderStatus("0");
               order.setOrderId(orderId);
               userOrderService.updateById(order);
           }
           if (order != null && order.getOrderId() != null) {
               userOrderRecord.setStatus(0);
               userOrderRecord.setReason(null);
               userOrderRecordService.updateById(userOrderRecord);
               return R.ok();
           }else{
               return R.error("未成功下单,请重新下单");
           }
        }
        return R.error("未成功下单");
    }
    /**
     * 信息
     */
    @RequestMapping("/reOrder/{id}")
    @RequiresPermissions("redDuck:userorderrecord:info")
    public R reOrder(@PathVariable("id") Integer id){
        UserOrderRecordEntity userOrderRecord = userOrderRecordService.getById(id);
        UserOrderEntity userOrder = userOrderService.getById(userOrderRecord.getOrderId());
        if (userOrderRecord.getStatus() >= 1) {
            Map<String, String> params = (Map<String, String>) JSONObject.parse(userOrderRecord.getParams());
            String taskId = params.get("attribusteBaseId");
            OrderTaskEntity task = orderTaskService.getOne(new QueryWrapper<OrderTaskEntity>()
                    .eq(StringUtils.isNotBlank(taskId),"task_id", taskId));
            if (task != null && !task.getTaskType().contains("直播")){
                List<MultipartFile> files = new ArrayList<>();
                if (params.containsKey("advertInfoUrl")) {
                    try{
                        MultipartFile file = HttpUtils.down(params.get("advertInfoUrl"));
                        if (file != null ){
                            files = new ArrayList<>();
                            files.add(0, file);
                            params.put("QEcodeImg", "0");
                        }
                    }catch (IOException e){
                        throw new RRException("url错误");
                    }
                }
                UserOrderRecordEntity record = userOrderService.orderRedYa(getUserId(), params, files);
                if (record.getReason() != null) {
                    return R.error(record.getReason());
                }
                userOrderRecord.setStatus(0);
                userOrderRecord.setReason(null);
                userOrderRecordService.updateById(userOrderRecord);
                return R.ok();
            } else {
                UserOrderRecordEntity record = userOrderService.unifiedOrder(getUserId(), params);
                if (record.getReason() != null) {
                    return R.error(record.getReason());
                }
                userOrderRecord.setStatus(0);
                userOrderRecord.setReason(null);
                userOrderRecordService.updateById(userOrderRecord);
                return R.ok();
            }
        }
        return R.error("订单已完成，无需重新下单");
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("redDuck:userorderrecord:save")
    public R save(@RequestBody UserOrderRecordEntity userOrderRecord){
		userOrderRecordService.save(userOrderRecord);

        return R.ok();
    }


    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("redDuck:userorderrecord:update")
    public R update(@RequestBody UserOrderRecordEntity userOrderRecord){
		userOrderRecordService.updateById(userOrderRecord);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("redDuck:userorderrecord:delete")
    public R delete(@RequestBody Integer[] ids){
		userOrderRecordService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
