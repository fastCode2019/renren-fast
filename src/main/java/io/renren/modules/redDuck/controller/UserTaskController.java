package io.renren.modules.redDuck.controller;

import java.util.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.exception.RRException;
import io.renren.modules.redDuck.entity.OrderTaskEntity;
import io.renren.modules.redDuck.entity.UserOrderEntity;
import io.renren.modules.redDuck.service.OrderTaskService;
import io.renren.modules.sys.entity.SysUserEntity;
import io.renren.modules.sys.service.SysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.redDuck.entity.UserTaskEntity;
import io.renren.modules.redDuck.service.UserTaskService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-11-25 01:46:22
 */
@RestController
@RequestMapping("redDuck/usertask")
public class UserTaskController {
    @Autowired
    private UserTaskService userTaskService;
    @Autowired
    private OrderTaskService orderTaskService;
    @Autowired
    private SysUserService sysUserService;
    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("redDuck:usertask:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = userTaskService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("redDuck:usertask:info")
    public R info(@PathVariable("id") Integer id){
		UserTaskEntity userTask = userTaskService.getById(id);

        return R.ok().put("userTask", userTask);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("redDuck:usertask:save")
    public R save(@RequestBody Map<String, Object> params){

        Integer userId = (Integer) params.get("userId");
        List<Integer> taskIds = (List<Integer>) params.get("taskIds");
        List<UserTaskEntity> userTasks = new ArrayList<>();

        this.userTaskService.remove(new QueryWrapper<UserTaskEntity>().eq("user_id", userId));
        SysUserEntity sysUserEntity = this.sysUserService.getById(userId);
        Integer llz = null;
        for (Integer task: taskIds) {
            UserTaskEntity userTask = new UserTaskEntity();
            userTask.setCreateTime(new Date());
            userTask.setType(0);

            OrderTaskEntity ote = orderTaskService.getById(task);
            if(ote.getTaskType().contains("流量主")){
                if(llz != null){
                    continue;
                }
                llz = ote.getId();
                userTask.setType(1);
            }
            userTask.setTaskId(ote.getId());
            userTask.setTaskName(ote.getTaskName());
            userTask.setUserName(sysUserEntity.getUsername());
            userTask.setUserId(userId);
            userTasks.add(userTask);
        }

        this.userTaskService.saveBatch(userTasks);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("redDuck:usertask:update")
    public R update(@RequestBody UserTaskEntity userTask){
		userTaskService.updateById(userTask);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("redDuck:usertask:delete")
    public R delete(@RequestBody Integer[] ids){
		userTaskService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
