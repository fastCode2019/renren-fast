package io.renren.modules.redDuck.dao;

import io.renren.modules.redDuck.entity.UserOrderSEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-11-25 01:46:21
 */
@Mapper
public interface UserOrderSDao extends BaseMapper<UserOrderSEntity> {
	
}
