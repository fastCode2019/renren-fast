package io.renren.modules.redDuck.dao;

import io.renren.modules.redDuck.entity.UserTaskEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-11-25 01:46:22
 */
@Mapper
public interface UserTaskDao extends BaseMapper<UserTaskEntity> {
	
}
