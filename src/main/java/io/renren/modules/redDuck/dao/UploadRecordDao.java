package io.renren.modules.redDuck.dao;

import io.renren.modules.redDuck.entity.UploadRecordEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-11-19 23:10:03
 */
@Mapper
public interface UploadRecordDao extends BaseMapper<UploadRecordEntity> {
	
}
