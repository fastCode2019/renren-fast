package io.renren.modules.redDuck.dao;

import io.renren.modules.redDuck.entity.UserPaymentEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-11-11 21:31:37
 */
@Mapper
public interface UserPaymentDao extends BaseMapper<UserPaymentEntity> {
	
}
