package io.renren.modules.redDuck.dao;

import io.renren.modules.redDuck.entity.UserOrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-11-06 18:04:49
 */
@Mapper
public interface UserOrderDao extends BaseMapper<UserOrderEntity> {
	
}
