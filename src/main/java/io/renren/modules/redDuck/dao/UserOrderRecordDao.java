package io.renren.modules.redDuck.dao;

import io.renren.modules.redDuck.entity.UserOrderRecordEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-08 21:53:52
 */
@Mapper
public interface UserOrderRecordDao extends BaseMapper<UserOrderRecordEntity> {
	
}
