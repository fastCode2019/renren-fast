package io.renren.modules.redDuck.dao;

import io.renren.modules.redDuck.entity.OrderLiquidationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-03 09:49:53
 */
@Mapper
public interface OrderLiquidationDao extends BaseMapper<OrderLiquidationEntity> {
	
}
