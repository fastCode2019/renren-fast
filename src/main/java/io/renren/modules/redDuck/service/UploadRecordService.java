package io.renren.modules.redDuck.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.redDuck.entity.UploadRecordEntity;

import java.util.Map;

/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-11-19 23:10:03
 */
public interface UploadRecordService extends IService<UploadRecordEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

