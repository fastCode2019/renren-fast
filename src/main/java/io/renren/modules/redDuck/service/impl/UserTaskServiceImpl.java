package io.renren.modules.redDuck.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.redDuck.dao.UserTaskDao;
import io.renren.modules.redDuck.entity.UserTaskEntity;
import io.renren.modules.redDuck.service.UserTaskService;


@Service("userTaskService")
public class UserTaskServiceImpl extends ServiceImpl<UserTaskDao, UserTaskEntity> implements UserTaskService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<UserTaskEntity> page = this.page(
                new Query<UserTaskEntity>().getPage(params),
                new QueryWrapper<UserTaskEntity>()
                        .eq(params.get("userId") != null, "user_id", params.get("userId"))
                        .like(params.get("userName") != null, "user_name", params.get("userName"))
        );

        return new PageUtils(page);
    }

}