package io.renren.modules.redDuck.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.redDuck.entity.OrderLiquidationEntity;

import java.util.Map;

/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-03 09:49:53
 */
public interface OrderLiquidationService extends IService<OrderLiquidationEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

