package io.renren.modules.redDuck.service.impl;

import cn.hutool.core.date.DateUtil;
import io.renren.common.utils.*;
import io.renren.modules.redDuck.entity.UserOrderEntity;
import io.renren.modules.redDuck.entity.UserOrderRecordEntity;
import io.renren.modules.redDuck.service.OrderTaskService;
import io.renren.modules.redDuck.service.UserOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import io.renren.modules.redDuck.dao.UserOrderSDao;
import io.renren.modules.redDuck.entity.UserOrderSEntity;
import io.renren.modules.redDuck.service.UserOrderSService;
import org.springframework.web.multipart.MultipartFile;


@Service("userOrderSService")
public class UserOrderSServiceImpl extends ServiceImpl<UserOrderSDao, UserOrderSEntity> implements UserOrderSService {
    @Autowired
    private UserOrderService userOrderService;


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<UserOrderSEntity> page = this.page(
                new Query<UserOrderSEntity>().getPage(params),
                new QueryWrapper<UserOrderSEntity>()
                        .eq(params.get("userId") != null, "user_id", params.get("userId"))
                .eq(params.get("orderStatus") != null, "order_status", params.get("orderStatus"))
        );

        return new PageUtils(page);
    }

    public void stepOrder(UserOrderSEntity order){
        if(!canOrder(order)){
           return ;
        }
        Integer use = order.getUseDay() == null ? 0 : order.getUseDay();
        if(use >= order.getTotalDay()){
            order.setOrderStatus(2);
            updateById(order);
            return ;
        }
        Integer num = order.getOrderNum() / order.getTotalDay();
        use++;
        order.setUseDay(use);
        order.setUseNum(order.getUseNum() + num);

        Map<String, String> params = new HashMap();
        List<MultipartFile> files = new ArrayList<>();
        params.put("attribusteBaseId", "c783b2d8bbfa40d89a3331025cf72774");
        params.put("seconds","60");
        params.put("advertInfoReadNumMax", num + "");
        Date update = new Date();
        String label = DateUtil.format(update,"yyyyMMddHHmmss");
        String orderName = order.getOrderName() + "_" + order.getUserId() + "_" + use + "_" + label;
        String wppName = order.getOrderName() + "_" + order.getUserId();
        params.put("advertInfoTitle", orderName);
        params.put("wppName", wppName);
        if (order.getOrderImage() != null) {
            try{
                MultipartFile file = HttpUtils.down(order.getOrderImage() );
                if (file != null ){
                    files = new ArrayList<>();
                    files.add(0, file);
                    params.put("QEcodeImg", "0");
                }
            }catch (IOException e){
                return ;
            }
        }
        UserOrderRecordEntity record = userOrderService.orderRedYa(1l, ApiParamFix.FixRadYaParam(params, true), files);
        UserOrderEntity o = userOrderService.getById(record.getOrderId());
        if(o != null){
            o.setOrderSId(order.getId());
            userOrderService.updateById(o);
            if(order.getUseDay() == order.getTotalDay()){
                order.setOrderStatus(2);
            }else{
                order.setOrderStatus(1);
            }
            order.setUpdateTime(new Date());
            updateById(order);
        } else {
            use --;
            order.setUseDay(use);
            order.setUseNum(order.getUseNum() - num);
            updateById(order);
        }
    }

    public boolean canOrder(UserOrderSEntity order){
        List<UserOrderEntity> os = userOrderService.getBaseMapper().selectList(new QueryWrapper<UserOrderEntity>().eq("order_s_id", order.getId()));
        if(os.size() > 0){
            for (UserOrderEntity e: os ) {
                if (e.getOrderStatus().equals("1") || e.getOrderStatus().equals("3")) {
                    try {
                         userOrderService.orderDelete(e.getOrderId());
                    }catch (Exception ex){
                        log.error(ex.getMessage());
                    }
                }else{
                    return false;
                }
            }
            return true;
        }
        return true;
    }

}