package io.renren.modules.redDuck.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.redDuck.entity.UserOrderRecordEntity;

import java.util.Map;

/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-08 21:53:52
 */
public interface UserOrderRecordService extends IService<UserOrderRecordEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

