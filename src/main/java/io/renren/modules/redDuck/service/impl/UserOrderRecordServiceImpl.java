package io.renren.modules.redDuck.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.redDuck.dao.UserOrderRecordDao;
import io.renren.modules.redDuck.entity.UserOrderRecordEntity;
import io.renren.modules.redDuck.service.UserOrderRecordService;


@Service("userOrderRecordService")
public class UserOrderRecordServiceImpl extends ServiceImpl<UserOrderRecordDao, UserOrderRecordEntity> implements UserOrderRecordService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<UserOrderRecordEntity> page = this.page(
                new Query<UserOrderRecordEntity>().getPage(params),
                new QueryWrapper<UserOrderRecordEntity>().eq("user_id", params.get("userId"))
                        .like(params.get("orderName") != null,"order_name", params.get("orderName"))
                        .eq(params.get("status") != null, "status", params.get("status")).orderByDesc("create_time")
        );

        return new PageUtils(page);
    }

}