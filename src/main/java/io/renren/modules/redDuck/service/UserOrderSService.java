package io.renren.modules.redDuck.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.redDuck.entity.UserOrderSEntity;

import java.util.Map;

/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-11-25 01:46:21
 */
public interface UserOrderSService extends IService<UserOrderSEntity> {

    PageUtils queryPage(Map<String, Object> params);
    void stepOrder(UserOrderSEntity order);
}

