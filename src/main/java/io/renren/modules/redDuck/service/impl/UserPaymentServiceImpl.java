package io.renren.modules.redDuck.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.redDuck.dao.UserPaymentDao;
import io.renren.modules.redDuck.entity.UserPaymentEntity;
import io.renren.modules.redDuck.service.UserPaymentService;


@Service("userPaymentService")
public class UserPaymentServiceImpl extends ServiceImpl<UserPaymentDao, UserPaymentEntity> implements UserPaymentService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<UserPaymentEntity> page = this.page(
                new Query<UserPaymentEntity>().getPage(params),
                new QueryWrapper<UserPaymentEntity>()
                        .eq("user_id", params.get("userId"))
                        .eq(params.get("orderId") != null,"order_id", params.get("orderId"))
                        .like(params.get("orderName") != null,"order_name", params.get("orderName"))
                        .eq(params.get("type") != null,"type", params.get("type"))
                        .orderByDesc("create_time")
        );

        return new PageUtils(page);
    }

}