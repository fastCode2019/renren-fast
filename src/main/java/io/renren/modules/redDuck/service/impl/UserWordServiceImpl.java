package io.renren.modules.redDuck.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.redDuck.dao.UserWordDao;
import io.renren.modules.redDuck.entity.UserWordEntity;
import io.renren.modules.redDuck.service.UserWordService;


@Service("userWordService")
public class UserWordServiceImpl extends ServiceImpl<UserWordDao, UserWordEntity> implements UserWordService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<UserWordEntity> page = this.page(
                new Query<UserWordEntity>().getPage(params),
                new QueryWrapper<UserWordEntity>()
        );

        return new PageUtils(page);
    }

}