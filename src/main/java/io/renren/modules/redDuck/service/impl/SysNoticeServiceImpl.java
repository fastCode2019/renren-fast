package io.renren.modules.redDuck.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.redDuck.dao.SysNoticeDao;
import io.renren.modules.redDuck.entity.SysNoticeEntity;
import io.renren.modules.redDuck.service.SysNoticeService;


@Service("sysNoticeService")
public class SysNoticeServiceImpl extends ServiceImpl<SysNoticeDao, SysNoticeEntity> implements SysNoticeService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SysNoticeEntity> page = this.page(
                new Query<SysNoticeEntity>().getPage(params),
                new QueryWrapper<SysNoticeEntity>()
        );

        return new PageUtils(page);
    }

}