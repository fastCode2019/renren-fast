package io.renren.modules.redDuck.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.exception.RRException;
import io.renren.common.utils.PageUtils;
import io.renren.modules.redDuck.entity.UserOrderEntity;
import io.renren.modules.redDuck.entity.UserOrderRecordEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-11-06 18:04:49
 */
public interface UserOrderService extends IService<UserOrderEntity> {

    PageUtils queryPage(Map<String, Object> params);
    String orderTask(Long userId, String taskId, Map<String, String> params) throws RRException;
    UserOrderRecordEntity orderRedYa(Long userId, Map<String, String> params, List<MultipartFile> files) throws RRException;
    JSONObject orderDetail(String orderId) throws RRException;
    JSONObject orderUpdate(String orderId, Map<String, String> params, List<MultipartFile> files) throws RRException;
    String orderOffLine(String orderId, Integer userId) throws RRException;
    String orderDelete(String orderId, String appId, String appKey) throws RRException;
    String orderDelete(String orderId) throws RRException;
    JSONObject orderUsage(String taskId) throws RRException;
    UserOrderRecordEntity newOrder(Long userId, Map<String, String> params, List<MultipartFile> files);
    void toRedYa(UserOrderEntity order);
    UserOrderRecordEntity unifiedOrder(Long userId, Map<String, String> params);
    void checkOrderStatus();
    void tryBatchDeleteRedYaOrder(List<String> orderId) throws RRException;
    String fetchOrderId(String orderName);
}

