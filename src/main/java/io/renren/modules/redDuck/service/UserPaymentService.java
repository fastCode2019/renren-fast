package io.renren.modules.redDuck.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.redDuck.entity.UserPaymentEntity;

import java.util.Map;

/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-11-11 21:31:37
 */
public interface UserPaymentService extends IService<UserPaymentEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

