package io.renren.modules.redDuck.service.impl;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.redDuck.dao.OrderTaskDao;
import io.renren.modules.redDuck.entity.OrderTaskEntity;
import io.renren.modules.redDuck.service.OrderTaskService;


@Service("orderTaskService")
public class OrderTaskServiceImpl extends ServiceImpl<OrderTaskDao, OrderTaskEntity> implements OrderTaskService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {

        IPage<OrderTaskEntity> page = this.page(
                new Query<OrderTaskEntity>().getPage(params),
                new QueryWrapper<OrderTaskEntity>()
                        .eq(params.get("task_type") != null, "task_type", params.get("task_type"))
                        .in(params.get("taskIds") != null, "id", (List<Integer>)params.get("taskIds"))
        );

        return new PageUtils(page);
    }

}