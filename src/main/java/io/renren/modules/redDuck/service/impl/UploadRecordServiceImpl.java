package io.renren.modules.redDuck.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.redDuck.dao.UploadRecordDao;
import io.renren.modules.redDuck.entity.UploadRecordEntity;
import io.renren.modules.redDuck.service.UploadRecordService;


@Service("uploadRecordService")
public class UploadRecordServiceImpl extends ServiceImpl<UploadRecordDao, UploadRecordEntity> implements UploadRecordService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<UploadRecordEntity> page = this.page(
                new Query<UploadRecordEntity>().getPage(params),
                new QueryWrapper<UploadRecordEntity>()
                        .eq("user_id", params.get("userId"))
                        .orderByDesc("create_time")
        );

        return new PageUtils(page);
    }

}