package io.renren.modules.redDuck.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.redDuck.entity.UserTaskEntity;

import java.util.Map;

/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-11-25 01:46:22
 */
public interface UserTaskService extends IService<UserTaskEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

