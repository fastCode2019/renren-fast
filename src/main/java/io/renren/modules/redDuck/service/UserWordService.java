package io.renren.modules.redDuck.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.redDuck.entity.UserWordEntity;

import java.util.Map;

/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-03 09:29:25
 */
public interface UserWordService extends IService<UserWordEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

