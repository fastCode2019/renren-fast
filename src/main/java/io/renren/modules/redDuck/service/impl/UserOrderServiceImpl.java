package io.renren.modules.redDuck.service.impl;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import io.renren.common.exception.RRException;
import io.renren.common.utils.*;
import io.renren.modules.oss.cloud.OSSFactory;
import io.renren.modules.redDuck.entity.OrderTaskEntity;
import io.renren.modules.redDuck.entity.UserOrderRecordEntity;
import io.renren.modules.redDuck.entity.UserPaymentEntity;
import io.renren.modules.redDuck.service.OrderTaskService;
import io.renren.modules.redDuck.service.UserOrderRecordService;
import io.renren.modules.redDuck.service.UserPaymentService;
import io.renren.modules.sys.entity.SysUserEntity;
import io.renren.modules.sys.service.SysConfigService;
import io.renren.modules.sys.service.SysUserService;
import lombok.Data;
import lombok.SneakyThrows;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import io.renren.modules.redDuck.dao.UserOrderDao;
import io.renren.modules.redDuck.entity.UserOrderEntity;
import io.renren.modules.redDuck.service.UserOrderService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;



@Service("userOrderService")
public class UserOrderServiceImpl extends ServiceImpl<UserOrderDao, UserOrderEntity> implements UserOrderService {
    private String appId = null;
    private String appKey = null;
    private String base = "https://api.redya.cc";
    @Autowired
    private OrderTaskService orderTaskService;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private UserPaymentService userPaymentService;
    @Autowired
    private SysConfigService sysConfigService;
    @Autowired
    private UserOrderService userOrderService;
    @Autowired
    private UserOrderRecordService userOrderRecordService;


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<UserOrderEntity> page = this.page(
                new Query<UserOrderEntity>().getPage(params),
                new QueryWrapper<UserOrderEntity>()
                        .eq("user_id", params.get("userId"))
                        .eq(params.get("orderStatus") != null, "order_status", params.get("orderStatus"))
                        .eq(params.get("orderSId") != null, "order_s_id", params.get("orderSId"))
                        .like(params.get("orderName") != null, "order_name", params.get("orderName"))
                        .orderByDesc("create_time")
        );
        for (UserOrderEntity order : page.getRecords()) {
            order.setOrderDetail(orderDetail(order.getOrderId()));
        }
        return new PageUtils(page);
    }
    @Transactional
    @Override
    public String orderTask(Long userId, String taskId, Map<String, String> params) throws RRException {
        appId = sysConfigService.getValue("appId");
        appKey = sysConfigService.getValue("appKey");
        OrderTaskEntity task = orderTaskService.getOne( new QueryWrapper<OrderTaskEntity>()
                .eq(StringUtils.isNotBlank(taskId),"task_id", taskId));
        SysUserEntity user = sysUserService.getById(userId);
        Long amount = user.getAmount();
        Integer num = Integer.parseInt(params.get("advertInfoReadNumMax"));
        Integer totalPrice = task.getTaskPrice() * num;
        if(amount - totalPrice >= 0){
            user.setAmount(amount - totalPrice);
        }else{
            throw new RRException("余额不足,请联系管理员充值");
        }
        sysUserService.updateById(user);


        String url = base + "/api/advert/openApi/TaskInfoCreate";
        params.put("appId", appId);
        params.put("appKey", appKey);
        if (taskId != null){
            params.put("attribusteBaseId", taskId);
        }
        String result = HttpUtils.post(url, params);
        JSONObject json = JSON.parseObject(result);



        if(json.getInteger("code") == 10001){
            return json.getString("data");
        }else{
            throw new RRException(json.getString("message"));
        }
    }

    @Override
    public UserOrderRecordEntity orderRedYa(Long userId, Map<String, String> params, List<MultipartFile> files) throws RRException {
        long count = userOrderService.count(new QueryWrapper<UserOrderEntity>().eq("order_name", params.get("advertInfoTitle")).eq("user_id", userId));
        if (count > 0 || params.get("advertInfoTitle") == null){
            throw new RRException("订单已存在");
        }
        String taskId = params.get("attribusteBaseId");
        appId = sysConfigService.getValue("appId");
        if(!params.containsKey("advertInfoUid")){
            String advertInfoUid = sysConfigService.getValue("advertInfoUid");
            if(advertInfoUid == null){
                params.put("advertInfoUid", appId);
            }else{
                params.put("advertInfoUid", advertInfoUid);
            }
        }
        if (!params.containsKey("advertInfoReadNumMax")) {
            throw new RRException("advertInfoReadNumMax, 不能为空");
        }
        Integer num = Integer.parseInt(params.get("advertInfoReadNumMax"));
        String url = "https://api.redya.cc/api/advert/taskinfo/TaskInfoCreate";
        Map<String, MultipartFile> fileMap = fileMapBuild(params, files);
        String orderId = null;
        JSONObject json = null;
        // 本地下单
        OrderFromResult orderResult = this.orderLocal(userId, taskId, num, params);
        UserOrderRecordEntity record = createRecord(orderResult.order, null);
        synchronized(this){ // 防止并发下单
            try {
                params.put("wppName", params.get("advertInfoTitle"));
                String result = HttpUtils.post(url, params, fileMap);
                json = JSON.parseObject(result);
            }catch (Exception e){
                record.setStatus(2);
                record.setReason("下单超时, 请检查订单是否成功");
            }
        }
        // 下单未成功
        if (json != null && json.getInteger("code") != 10001) {
            String message = json.getString("message");
            record.setStatus(1);
            record.setReason(message);
            if (params.get("advertInfoUrl") == null || params.get("advertInfoUrl").isEmpty()){
                String ossUrl = uploadOss(files);
                if (ossUrl != null){
                    params.put("advertInfoUrl", ossUrl);
                    record.setParams(JSONObject.toJSONString(params));
                }
            }
            sysUserService.subAmount(userId, -orderResult.getPayment().getAmount());
            userOrderRecordService.save(record);
            return record;
        }else if(json != null) {
            orderId = json.getString("data");
        }

        try {
            orderResult.order.setOrderId(orderId);
            orderResult.payment.setOrderId(orderId);
            if (orderId == null) {
                orderResult.order.setOrderStatus("-1");
            }
            userOrderService.save(orderResult.order);
            orderResult.payment.setUserOrderId(orderResult.order.getId());
            userPaymentService.save(orderResult.payment);
        }catch (Exception e){
            log.error("下单保存失败, 下线订单");
            record.setStatus(1);
            record.setReason("下单失败: " + e.getMessage());
            sysUserService.subAmount(userId, -orderResult.getPayment().getAmount());
        }
        record.setRedYaId(orderId);
        record.setOrderId(orderResult.order.getId());
        if ((params.get("advertInfoUrl") == null|| params.get("advertInfoUrl").isEmpty())){
            String ossUrl = uploadOss(files);
            if (ossUrl != null){
                params.put("advertInfoUrl", ossUrl);
                record.setParams(JSONObject.toJSONString(params));
            }
        }
        orderResult.order.setOrderParam(JSONObject.toJSONString(params));
        updateById(orderResult.order);
        userOrderRecordService.save(record);
        return record;
    }

    @Override
    public UserOrderRecordEntity newOrder(Long userId, Map<String, String> params, List<MultipartFile> files) throws RRException {
        long count = userOrderService.count(new QueryWrapper<UserOrderEntity>().eq("order_name", params.get("advertInfoTitle")).eq("user_id", userId));
        if (count > 0 || params.get("advertInfoTitle") == null){
            throw new RRException("订单已存在");
        }
        String taskId = params.get("attribusteBaseId");
        appId = sysConfigService.getValue("appId");
        if(!params.containsKey("advertInfoUid")){
            String advertInfoUid = sysConfigService.getValue("advertInfoUid");
            if(advertInfoUid == null){
                params.put("advertInfoUid", appId);
            }else{
                params.put("advertInfoUid", advertInfoUid);
            }
        }
        if (!params.containsKey("advertInfoReadNumMax")) {
            throw new RRException("advertInfoReadNumMax, 不能为空");
        }
        Integer num = Integer.parseInt(params.get("advertInfoReadNumMax"));
        // 本地下单
        OrderFromResult orderResult = this.orderLocal(userId, taskId, num, params);
        UserOrderRecordEntity record = createRecord(orderResult.order, null);
        params.put("wppName", orderResult.order.getOrderName());
        if (params.get("advertInfoUrl") == null || params.get("advertInfoUrl").isEmpty()){
            String ossUrl = uploadOss(files);
            if (ossUrl != null){
                params.put("advertInfoUrl", ossUrl);
                record.setParams(JSONObject.toJSONString(params));

            }
        }
        orderResult.order.setOrderParam(JSONObject.toJSONString(params));
        record.setOrderName(orderResult.order.getOrderName());
        record.setOrderId(orderResult.order.getId());
        try {
            orderResult.order.setOrderStatus("-1");
            userOrderService.save(orderResult.order);
            orderResult.payment.setUserOrderId(orderResult.order.getId());
            userPaymentService.save(orderResult.payment);
        }catch (Exception e){
            log.error("下单保存失败, 下线订单");
            record.setStatus(1);
            record.setReason("下单失败: " + e.getMessage());
            sysUserService.subAmount(userId, -orderResult.getPayment().getAmount());
        }
        record.setOrderId(orderResult.order.getId());
        userOrderRecordService.save(record);
        return record;
    }

    @Override
    public void toRedYa(UserOrderEntity order) {
        Map<String, MultipartFile> fileMap = new HashMap();
        Map<String, String> params = JSONObject.parseObject(order.getOrderParam(), Map.class);
        String orderId = fetchOrderId(order.getOrderName());
        JSONObject orderJSON = orderDetail(orderId);
        if (orderJSON != null) {
            String wppName = orderJSON.getString("advertInfoWpp");
            if (order.getOrderStatus().equals("-1") && wppName.equals(params.get("wppName"))){
                order.setOrderStatus("0");
                order.setUpdateTime(DateUtil.date());
                order.setOrderId(orderId);
                order.setRedyaDetail(orderJSON.toJSONString());
                updateById(order);
                UserPaymentEntity payment = userPaymentService.getOne(new QueryWrapper<UserPaymentEntity>().eq("user_order_id", order.getId()).eq("type", 0));
                if (payment != null) {
                    payment.setOrderId(orderId);
                    userPaymentService.updateById(payment);
                }
                UserOrderRecordEntity record = userOrderRecordService.getOne(new QueryWrapper<UserOrderRecordEntity>().eq("order_id", order.getId()));
                if (record != null) {
                    record.setStatus(0);
                    userOrderRecordService.updateById(record);
                }
                return ;
            }
        }
        try {
            MultipartFile file = HttpUtils.down(params.get("advertInfoUrl"));
            if (file != null ){
                List<MultipartFile> files = new ArrayList<>();
                files.add(0, file);
                params.put("QEcodeImg", "0");
                fileMap = fileMapBuild(params, files);
            }
        } catch (IOException e){
            return ;
        }
        params.put("wppName", order.getOrderName());
        String url = "https://api.redya.cc/api/advert/taskinfo/TaskInfoCreate";
        JSONObject json = JSON.parseObject(HttpUtils.post(url, params, fileMap));
        if (json != null && json.getInteger("code") == 10001) {
            order.setOrderStatus("0");
            order.setUpdateTime(DateUtil.date());
            order.setOrderId(json.getString("data"));
            order.setRedyaDetail(json.toJSONString());
            UserPaymentEntity payment = userPaymentService.getOne(new QueryWrapper<UserPaymentEntity>().eq("user_order_id", order.getId()).eq("type", 0));
            if (payment != null) {
                payment.setOrderId(orderId);
                userPaymentService.updateById(payment);
            }
            UserOrderRecordEntity record = userOrderRecordService.getOne(new QueryWrapper<UserOrderRecordEntity>().eq("order_id", order.getId()));
            if (record != null) {
                record.setStatus(0);
                userOrderRecordService.updateById(record);
            }
            updateById(order);
        } else {
            UserOrderRecordEntity record = userOrderRecordService.getOne(new QueryWrapper<UserOrderRecordEntity>().eq("order_id", order.getId()));
            if (record != null) {
                record.setStatus(1);
                record.setReason(json.getString("message"));
                userOrderRecordService.updateById(record);
            }
            order.setOrderStatus("2");
            order.setRedyaDetail(json.toJSONString());
            updateById(order);
        }
    }
    public UserOrderRecordEntity createRecord(UserOrderEntity order, String reason){
        UserOrderRecordEntity record = new UserOrderRecordEntity();
        record.setCreateTime(DateUtil.date());
        record.setUserId(order.getUserId());
        record.setStatus(0);
        record.setReason(reason);
        record.setOrderName(order.getOrderName());
        record.setOrderId(order.getId());
        record.setParams(JSONObject.toJSONString(order.getOrderParam()));
        return record;
    }

    public String uploadOss(List<MultipartFile> files){
        if(files.size() == 0)
            return null;
        try {
            String suffix = files.get(0).getOriginalFilename().substring(files.get(0).getOriginalFilename().lastIndexOf("."));
            String url = OSSFactory.build().uploadSuffix(files.get(0).getBytes(), suffix);
            return url;
        }catch (IOException e){
            return null;
        }
    }

    public Map<String, MultipartFile> fileMapBuild(Map<String, String> params, List<MultipartFile> files){
        Map<String, MultipartFile> fileMap = new HashMap();
        String[] fileFields = {"QEcodeImg"};
        List<String> fileField =  Arrays.asList(fileFields);
        for (Map.Entry<String, String> param : params.entrySet()) {
            if(fileField.contains(param.getKey())){
                String index = param.getValue();
                if(files.size() > 0 ){
                    fileMap.put(param.getKey(), files.get(Integer.parseInt(index)));
                }else{
                    throw new RRException("文件不能为空");
                }
            }
        }
        return fileMap;
    }



    @Transactional
    @Override
    public JSONObject orderUpdate(String orderId, Map<String, String> params, List<MultipartFile> files) throws RRException {

        UserOrderEntity order = this.getOne(new QueryWrapper<UserOrderEntity>().eq("order_id", orderId));
        long count = userOrderService.count(new QueryWrapper<UserOrderEntity>().eq("order_name", params.get("advertInfoTitle")).eq("user_id", order.getUserId()));
        if (order == null)
            throw new RRException("订单信息不存在");
        if (!order.getOrderStatus().equals("1")) {
            throw new RRException("订单未下线");
        }
        if (count > 0 && !order.getOrderName().equals(params.get("advertInfoTitle"))){
            throw new RRException("订单已存在");
        }
        String url = base + "/api/advert/taskinfo/TaskInfoUpdate";
        params.put("advertInfoUid", sysConfigService.getValue("appId"));
        params.put("advertInfoId", orderId);

        if(!params.containsKey("advertInfoReadNumMax")){
            throw new RRException("advertInfoReadNumMax, 不能为空");
        }
        Integer num = Integer.parseInt(params.get("advertInfoReadNumMax"));


        // 本地下单
        OrderFromResult orderResult = this.orderLocal(order.getUserId().longValue(), order.getTaskId(), num, params);
        Map<String, MultipartFile> fileMap = fileMapBuild(params, files);
        String result = HttpUtils.post(url, params, fileMap);

        JSONObject json = JSON.parseObject(result);
        if(json.getInteger("code") == 10001){
            try {
                orderResult.payment.setOrderId(orderId);
                order.setOrderStatus("0");
                order.setMaxNum(num);
                order.setOrderName(orderResult.order.getOrderName());
                order.setOrderPrice(orderResult.order.getOrderPrice());
                order.setUpdateTime(new Date());
                userOrderService.updateById(order);
                userPaymentService.save(orderResult.payment);
            }catch (Exception e){
                log.error("修改订单失败, 下线订单");

                orderOffLine(orderId, order.getUserId());
            }
            return json.getJSONObject("data");
        }else{
            throw new RRException(json.getString("message"));
        }
    }

    @Transactional
    private OrderFromResult orderLocal(Long userId, String taskId, Integer num, Map<String, String> params){
        OrderTaskEntity task = orderTaskService.getOne(new QueryWrapper<OrderTaskEntity>()
                .eq(StringUtils.isNotBlank(taskId),"task_id", taskId));
        OrderFromResult result = new OrderFromResult();
        UserOrderEntity order = new UserOrderEntity();
        UserPaymentEntity payment = new UserPaymentEntity();
        order.setTaskType(task.getTaskName());
        if (task == null) {
            throw new RRException("任务Id错误");
        }
        Integer totalPrice = task.getTaskPrice() * num;
        if (task.getTaskType().contains("直播") || task.getTaskName().contains("直播")) {
            if(params.get("clickRate") == null){
                throw  new RRException("缺少参数clickRate");
            }
            order.setTaskType("直播");
            totalPrice = task.getTaskPrice() * num * Integer.parseInt(params.get("clickRate"));
        }
        SysUserEntity user = sysUserService.getById(userId);
        sysUserService.subAmount(userId, totalPrice.longValue());
        payment.setOldAmount(user.getAmount());
        payment.setUserId(userId.intValue());
        payment.setCreateTime(new Date());
        payment.setType(0);
        payment.setTaskId(taskId);
        payment.setAmount(totalPrice.longValue());
        payment.setOrderName(params.get("advertInfoTitle"));
        order.setCreateTime(new Date());
        order.setUserId(userId.intValue());
        order.setOrderName(params.get("advertInfoTitle"));
        order.setOrderStatus("0");
        order.setTaskId(taskId);
        order.setOrderPrice(totalPrice);
        order.setMaxNum(num);
        order.setRealNum(num);
        order.setOrderParam(JSONObject.toJSONString(params));
        result.order = order;
        result.payment = payment;
        return result;
    }

    @Override
    public JSONObject orderDetail(String orderId) throws RRException {
        appId = sysConfigService.getValue("appId");
        appKey = sysConfigService.getValue("appKey");
        String url = base + "/api/advert/openApi/getTaskInfo";
        Map params = new HashMap();
        params.put("appId", appId);
        params.put("appKey", appKey);
        params.put("taskInfoId", orderId);
        String result = HttpUtils.post(url, params);
        JSONObject json = JSON.parseObject(result);
        if(json.getInteger("code") == 10001){
            return json.getJSONObject("data");
        }else{
            throw new RRException(json.getString("message"));
        }
    }


    public JSONObject orderDetail(String orderId, String appId, String appKey){
        String url = base + "/api/advert/openApi/getTaskInfo";
        Map params = new HashMap();
        params.put("appId", appId);
        params.put("appKey", appKey);
        params.put("taskInfoId", orderId);
        String result = HttpUtils.post(url, params);
        JSONObject json = JSON.parseObject(result);
        if(json.getInteger("code") == 10001){
            return json.getJSONObject("data");
        }else{
            throw new RRException(json.getString("message"));
        }
    }

    public void tryOrderOffLine(String orderId, Integer userId){
        try{
            orderOffLine(orderId, userId);
        }catch (Exception e){
            log.error("订单尝试下线失败");
        }
    }
    @Override
    public String orderOffLine(String orderId, Integer userId) throws RRException {
        appId = sysConfigService.getValue("appId");
        appKey = sysConfigService.getValue("appKey");
        String url = base + "/api/advert/openApi/offlineTaskInfo";
        Map params = new HashMap();
        params.put("appId", appId);
        params.put("appKey", appKey);
        params.put("advertInfoId", orderId);
        String result = HttpUtils.post(url, params);
        JSONObject json = JSON.parseObject(result);
        if(json.getInteger("code") == 10001){
            return json.getString("data");
        }else{
            throw new RRException(json.getString("message"));
        }
    }


    @Override
    public String orderDelete(String orderId, String appId, String appKey) throws RRException {

        String url = base + "/api/advert/openApi/deleteTaskInfo";
        Map params = new HashMap();
        params.put("appId", appId);
        params.put("appKey", appKey);
        params.put("advertInfoId", orderId);
        String result = HttpUtils.post(url, params);
        JSONObject json = JSON.parseObject(result);
        if(json.getInteger("code") == 10001){
            return json.getString("data");
        }else{
            throw new RRException(json.getString("message"));
        }
    }

    @Override
    public String orderDelete(String orderId) throws RRException {
        appId = sysConfigService.getValue("appId");
        appKey = sysConfigService.getValue("appKey");
        return orderDelete(orderId, appId, appKey);
    }

    @Override
    public JSONObject orderUsage(String taskId) throws RRException {
        appId = sysConfigService.getValue("appId");
        appKey = sysConfigService.getValue("appKey");
        String url = base + "/api/advert/openApi/getTaskTypeSurplus";
        Map params = new HashMap();
        params.put("appId", appId);
        params.put("appKey", appKey);
        params.put("attribusteBaseId", taskId);
        String result = HttpUtils.post(url, params);
        JSONObject json = JSON.parseObject(result);
        if(json.getInteger("code") == 10001){
            return json.getJSONObject("data");
        }else{
            throw new RRException(json.getString("message"));
        }
    }

    @Override
    public UserOrderRecordEntity unifiedOrder(Long userId, Map<String, String> params) {
        long count = userOrderService.count(new QueryWrapper<UserOrderEntity>().eq("order_name", params.get("advertInfoTitle")).eq("user_id", userId));
        if (count > 0 || params.get("advertInfoTitle") == null){
            throw new RRException("订单已存在");
        }
        String taskId = params.get("attribusteBaseId");
        if (!params.containsKey("advertInfoUid")) {
            appId = sysConfigService.getValue("appId");
            params.put("advertInfoUid", appId);
        }
        if (!params.containsKey("attribusteBaseId")) {
            throw new RRException("attribusteBaseId, 不能为空");
        }
        if (!params.containsKey("advertInfoReadNumMax")) {
            throw new RRException("advertInfoReadNumMax, 不能为空");
        }
        Integer num = Integer.parseInt(params.get("advertInfoReadNumMax"));

        OrderTaskEntity task = orderTaskService.getOne(new QueryWrapper<OrderTaskEntity>()
                .eq(StringUtils.isNotBlank(taskId),"task_id", taskId));
        if (task == null) {
            throw new RRException("attribusteBaseId, 错误");
        }
        UserOrderRecordEntity record = new UserOrderRecordEntity();
        record.setCreateTime(new Date());
        record.setUserId(userId.intValue());
        record.setStatus(0);
        record.setParams(JSONObject.toJSONString(params));
        JSONObject json;
        String orderId = null;
        // 本地下单
        OrderFromResult orderResult = this.orderLocal(userId, taskId, num, params);

        try {
            json = RedYaApi.post(task.getTaskCreateApi(), params);
        }catch (Exception e){
            json = null;
            record.setStatus(2);
            record.setReason("下单超时, 请检查订单是否成功");
        }
        // 下单未成功
        if (json != null && json.getInteger("code") != 0) {
            String message = json.getString("msg");
            record.setOrderName(params.get("advertInfoTitle"));
            record.setStatus(1);
            record.setReason(message);
            sysUserService.subAmount(userId, -orderResult.getPayment().getAmount());
            userOrderRecordService.save(record);
            return record;
        }else if(json != null) {
            orderId = json.getString("data");
        }
        record.setOrderName(orderResult.order.getOrderName());
        try {
            orderResult.order.setOrderId(orderId);
            orderResult.payment.setOrderId(orderId);
            userOrderService.save(orderResult.order);
            orderResult.payment.setUserOrderId(orderResult.order.getId());
            userPaymentService.save(orderResult.payment);
            record.setOrderId(orderResult.order.getId());

        }catch (Exception e){
            log.error("下单保存失败, 下线订单");
            record.setStatus(1);
            record.setReason("下单失败: " + e.getMessage());
            sysUserService.subAmount(userId, -orderResult.getPayment().getAmount());
        }
        userOrderRecordService.save(record);
        return record;
    }

    @Override
    public void checkOrderStatus() throws RRException {
        appId = sysConfigService.getValue("appId");
        appKey = sysConfigService.getValue("appKey");
        List<UserOrderEntity> list = this.getBaseMapper().selectList(new QueryWrapper<UserOrderEntity>().eq("order_status", "0").or().eq("order_status", "2"));
        for ( UserOrderEntity order: list ) {
            JSONObject json = null;
            if (order.getOrderId() != null) {
                try {
                    json = orderDetail(order.getOrderId(), appId, appKey);
                } catch (Exception e) {
                    log.error(order.getOrderName() + e.getMessage());
                }
                if (json == null) { // 不能确定
                    log.error(order.getOrderName() + "订单信息为空");
                    OrderOffline(order, order.getRealNum());
                    continue;
                }
            } else {
                OrderOffline(order, order.getRealNum());
                continue;
            }
            try {
                Integer status = json.getInteger("advertInfoIsdelete");
                Integer real = json.getInteger("advertInfoReadNumReal");
                Integer max = json.getInteger("advertInfoReadNumMax");
                if (max.equals(order.getMaxNum())) {
                    Integer num = max - real;
                    order.setRealNum(num);
                    order.setMaxNum(max);
                    order.setUpdateTime(new Date());
                    order.setRedyaDetail(json.toJSONString());
                    if (status == 1) { // 订单下线
                        OrderOffline(order, num);
                    } else {
                        offlineRedYaOrderAndUserOrder(order, num);
                    }
                } else {
                    OrderOffline(order, order.getRealNum());
                    if(order.getOrderStatus().equals("1")){
                        UserOrderRecordEntity record = createRecord(order, "订单异常，下线");
                        record.setStatus(1);
                        userOrderRecordService.save(record);
                    }
                }
            }catch (Exception e){
                log.error("订单下线失败, " + e.getMessage());
            }
        }
    }

    @Override
    public void tryBatchDeleteRedYaOrder(List<String> orderId) throws RRException {
        appId = sysConfigService.getValue("appId");
        appKey = sysConfigService.getValue("appKey");
        for (String id: orderId) {
            try {
                orderDelete(id, appId, appKey);
            }catch (Exception e){
                log.error(id+ "---->删除失败");
            }
        }
    }

    private void offlineRedYaOrderAndUserOrder(UserOrderEntity order, Integer num){
        if (num == 0) {
            orderOffLine(order.getOrderId(), order.getUserId());
            order.setOrderStatus("1");
        }
        updateById(order);
    }
    private void orderRefund(UserOrderEntity order){
        OrderTaskEntity task = orderTaskService.getOne(new QueryWrapper<OrderTaskEntity>()
                .eq("task_id", order.getTaskId()));
        if(order.getOrderId() == null){
            Integer total = task.getTaskPrice() * order.getMaxNum();
            sysUserService.subAmount(order.getUserId().longValue(), -total.longValue());
        }
    }

    private void OrderOffline(UserOrderEntity order, Integer num){
        if (order.getOrderStatus().equals("2")) {
            order.setOrderStatus("1");
            if (num != null && num > 0){
                OrderTaskEntity task = orderTaskService.getOne(new QueryWrapper<OrderTaskEntity>()
                        .eq("task_id", order.getTaskId()));
                if (task.getTaskType().contains("直播") || task.getTaskName().contains("直播")) {
                    // TODO
                } else {
                    SysUserEntity user = sysUserService.getById(order.getUserId());
                    Integer total = task.getTaskPrice() * num;
                    UserPaymentEntity payment = new UserPaymentEntity();
                    payment.setAmount(total.longValue());
                    payment.setUserId(order.getUserId());
                    payment.setType(2);
                    payment.setOldAmount(user.getAmount());
                    payment.setUserOrderId(order.getId());
                    payment.setCreateTime(new Date());
                    payment.setOrderId(order.getOrderId());
                    payment.setOrderName(order.getOrderName());
                    sysUserService.subAmount(user.getUserId(), -total.longValue());
                    userPaymentService.save(payment);
                }
            }
        }else if(order.getOrderStatus().equals("0")){
            order.setOrderStatus("2");
        }
        updateById(order);
    }

    public String tryFetchOrderId(String orderName, String appId) {
       try{
          return fetchOrderId(orderName, appId);
       }catch (Exception e){
            return null;
       }
    }
    public String fetchOrderId(String orderName) {
        appId = sysConfigService.getValue("appId");
        return fetchOrderId(orderName, appId);
    }
    private String fetchOrderId(String orderName, String appId){
        Map<String, String> map = new HashMap<>();
        map.put("advertInfoUid", appId);
        map.put("page","1");
        map.put("limit","100");
        map.put("search", orderName);
        map.put("state","0");
        JSONObject json = RedYaApi.post("https://api.redya.cc/api/advert/advertInfo/list", map);
        if(json.getInteger("code") == 0){
            JSONObject data = json.getJSONObject("data");
            if (data != null){
                JSONArray array = data.getJSONArray("data");
                if(array.size() > 0){
                    return array.getJSONObject(0).getString("advertInfoId");
                }else{
                    return null;
                }
            }else{
                return null;
            }
        }else{
            throw new RRException(json.getString("msg"));
        }
    }

    @SneakyThrows
    public static void main(String args[]) {

//        Map<String,String> map = new HashMap();
//        map.put("advertInfoReadNumMax", "1");
//        map.put("advertInfoUid", "b6700d8bca3b42a8808a34f2410bc562");
//        map.put("attribusteBaseId", "b9330fca535c44fea95d41df2aef0709");
//        map.put("advertInfoTitle", "test2");
//        map.put("wppName", "test2");
//        map.put("seconds", "1");
//
//        File file = new File("C:\\Users\\xiaobai\\Pictures/test0.jpg");
//        FileItem fileItem = new DiskFileItem(
//                "formFieldName",//form表单文件控件的名字随便起
//                Files.probeContentType(file.toPath()),//文件类型
//                false, //是否是表单字段
//                file.getName(),//原始文件名
//                (int) file.length(),//Interger的最大值可以存储两部1G的电影
//                file.getParentFile());//文件会在哪个目录创建
//
//        IOUtils.copy(new FileInputStream(file), fileItem.getOutputStream());
//        MultipartFile cMultiFile = new CommonsMultipartFile(fileItem);
//        System.out.println(cMultiFile.getOriginalFilename());
//        String url = "https://api.redya.cc/api/advert/taskinfo/TaskInfoCreate";
//        Map<String, MultipartFile> fileMap = new HashMap();
//        fileMap.put("QEcodeImg", cMultiFile);
        JSONObject object = new UserOrderServiceImpl().orderDetail("73d6b13d148f4f409152f9cc8b37c958","175543d72f874353a2df64eee7a8cd4a","28c0ea7b6a2b4532aa8f32ce7f197293");
        System.out.println(  object.getInteger("advertInfoIsdelete"));
    }


}

@Data
class OrderFromResult {
    SysUserEntity user;
    UserOrderEntity order;
    UserPaymentEntity payment;
}
